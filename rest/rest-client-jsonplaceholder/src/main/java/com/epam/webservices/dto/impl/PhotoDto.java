package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.IPhotoDto;
import com.epam.webservices.entity.Photo;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class PhotoDto implements IPhotoDto {
    private final IDataManager config;

    public PhotoDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public List<Photo> getPhotosByAlbumId(int albumId) {
        var link = config.getString("jsonplaceholder.photos.link");
        return Unirest.get(link).queryString("albumId", albumId).asObject(new GenericType<List<Photo>>() {}).getBody();
    }
}
