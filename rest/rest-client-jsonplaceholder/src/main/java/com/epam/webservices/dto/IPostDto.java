package com.epam.webservices.dto;

import com.epam.webservices.entity.Post;

import java.util.List;

public interface IPostDto {
    List<Post> getPosts();

    Post getPostById(int id);

    List<Post> getPostsByUserId(int userId);
}
