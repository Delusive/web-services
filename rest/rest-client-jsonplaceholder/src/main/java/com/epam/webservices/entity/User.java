package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class User extends Entity {
    @SerializedName("id")
    public final int ID;
    @SerializedName("name")
    public final String NAME;
    @SerializedName("username")
    public final String USERNAME;
    @SerializedName("email")
    public final String EMAIL;
    @SerializedName("address")
    public final Address ADDRESS;
    @SerializedName("phone")
    public final String PHONE;
    @SerializedName("website")
    public final String WEBSITE;
    @SerializedName("company")
    public final Company COMPANY;

    public User(int id, String name, String username, String email, Address address, String phone, String website, Company company) {
        ID = id;
        NAME = name;
        USERNAME = username;
        EMAIL = email;
        ADDRESS = address;
        PHONE = phone;
        WEBSITE = website;
        COMPANY = company;
    }


    public class Address {
        @SerializedName("street")
        public final String STREET;
        @SerializedName("suite")
        public final String SUITE;
        @SerializedName("city")
        public final String CITY;
        @SerializedName("zipcode")
        public final String ZIPCODE;
        @SerializedName("geo")
        public final Geo GEO;

        public Address(String street, String suite, String city, String zipcode, Geo geo) {
            STREET = street;
            SUITE = suite;
            CITY = city;
            ZIPCODE = zipcode;
            GEO = geo;
        }

        public class Geo {
            @SerializedName("lat")
            public final String LAT;
            @SerializedName("lng")
            public final String LNG;

            public Geo(String lat, String lng) {
                LAT = lat;
                LNG = lng;
            }
        }
    }

    public class Company {
        @SerializedName("name")
        public final String NAME;
        @SerializedName("catchPhrase")
        public final String CATCH_PHRASE;
        @SerializedName("bs")
        public final String BS;

        public Company(String name, String catchPhrase, String bs) {
            NAME = name;
            CATCH_PHRASE = catchPhrase;
            BS = bs;
        }
    }

    @Override
    public String toString() {
        return "ID: " + ID +
                ", NAME: " + NAME +
                ", USERNAME: " + USERNAME +
                ", EMAIL: " + EMAIL +
                ", ADDRESS: " + ADDRESS.CITY + ", " + ADDRESS.STREET + ", " + ADDRESS.SUITE +
                ", PHONE: " + PHONE +
                ", WEBSITE: " + WEBSITE +
                ", COMPANY: " + COMPANY.NAME + ", " + COMPANY.CATCH_PHRASE + ", " + COMPANY.BS;
    }
}
