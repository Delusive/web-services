package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Post extends Entity {
    @SerializedName("userId")
    public final int USER_ID;
    @SerializedName("id")
    public final int ID;
    @SerializedName("title")
    public final String TITLE;
    @SerializedName("body")
    public final String BODY;

    public Post(int userId, int id, String title, String body) {
        USER_ID = userId;
        ID = id;
        TITLE = title;
        BODY = body;
    }

    @Override
    public String toString() {
        return "ID: " + ID + ", TITLE: " + TITLE + ", BODY: " + BODY;
    }
}
