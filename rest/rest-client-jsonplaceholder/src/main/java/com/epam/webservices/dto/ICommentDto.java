package com.epam.webservices.dto;

import com.epam.webservices.entity.Comment;

import java.util.List;

public interface ICommentDto {
    List<Comment> getComments();

    List<Comment> getCommentsByPostId(int postId);

}
