package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.ICommentDto;
import com.epam.webservices.entity.Comment;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class CommentDto implements ICommentDto {
    private final IDataManager config;

    public CommentDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public List<Comment> getComments() {
        var link = config.getString("jsonplaceholder.comments.link");
        return Unirest.get(link).asObject(new GenericType<List<Comment>>() {}).getBody();
    }

    @Override
    public List<Comment> getCommentsByPostId(int postId) {
        var link = config.getString("jsonplaceholder.comments.link");
        return Unirest.get(link).queryString("postId", postId).asObject(new GenericType<List<Comment>>() {}).getBody();
    }
}
