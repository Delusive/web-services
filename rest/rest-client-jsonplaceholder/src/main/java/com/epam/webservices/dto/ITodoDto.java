package com.epam.webservices.dto;

import com.epam.webservices.entity.Todo;

import java.util.List;

public interface ITodoDto {
    List<Todo> getTodosByUserId(int userId);
}
