package com.epam.webservices;

import com.epam.webservices.dto.*;
import com.epam.webservices.dto.impl.*;
import com.epam.webservices.manager.IDataManager;
import com.epam.webservices.manager.impl.PropertiesManager;
import com.epam.webservices.ui.IUserInterface;
import com.epam.webservices.ui.impl.ConsoleUserInterface;
import kong.unirest.GsonObjectMapper;
import kong.unirest.Unirest;

public class Main {

    public static void main(String[] args) {
        final IDataManager config = new PropertiesManager("/client.properties");
        final IDataManager messages = new PropertiesManager("/messages_ru.properties");
        final IUserDto userDto = new UserDto(config);
        final ITodoDto todoDto = new TodoDto(config);
        final IPhotoDto photoDto = new PhotoDto(config);
        final IAlbumDto albumDto = new AlbumDto(config);
        final IPostDto postDto = new PostDto(config);
        final ICommentDto commentDto = new CommentDto(config);
        final IUserInterface userInterface = new ConsoleUserInterface(messages, userDto, todoDto, photoDto, albumDto, postDto, commentDto);
        //Danger zone begin
        Unirest.config().setObjectMapper(new GsonObjectMapper());
        //Danger zone end
        userInterface.delegate();
    }
}
