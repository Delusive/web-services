package com.epam.webservices.dto;

import com.epam.webservices.entity.Album;

import java.util.List;

public interface IAlbumDto {
    List<Album> getAlbumsByUserId(int userId);
}
