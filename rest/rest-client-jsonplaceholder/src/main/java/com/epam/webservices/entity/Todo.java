package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Todo extends Entity {
    @SerializedName("userId")
    public final int USER_ID;
    @SerializedName("id")
    public final int ID;
    @SerializedName("title")
    public final String TITLE;
    @SerializedName("completed")
    public final boolean IS_COMPLETED;

    public Todo(int user_id, int id, String title, boolean is_completed) {
        USER_ID = user_id;
        ID = id;
        TITLE = title;
        IS_COMPLETED = is_completed;
    }

    @Override
    public String toString() {
        return "TITLE: " + TITLE + ", STATUS: " + (IS_COMPLETED ? "completed" : "not completed");
    }
}
