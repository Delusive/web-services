package com.epam.webservices.dto;

import com.epam.webservices.entity.Photo;

import java.util.List;

public interface IPhotoDto {
    List<Photo> getPhotosByAlbumId(int albumId);
}
