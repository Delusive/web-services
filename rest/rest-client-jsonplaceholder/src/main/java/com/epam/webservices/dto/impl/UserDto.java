package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.IUserDto;
import com.epam.webservices.entity.User;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class UserDto implements IUserDto {
    private final IDataManager config;

    public UserDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public User getUserById(int userId) {
        var link = config.getString("jsonplaceholder.users.link");
        var result = Unirest.get(link).queryString("id", userId).asObject(new GenericType<List<User>>() {}).getBody();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public List<User> getUsers() {
        var link = config.getString("jsonplaceholder.users.link");
        return Unirest.get(link).asObject(new GenericType<List<User>>() {}).getBody();
    }
}
