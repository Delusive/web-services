package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Photo extends Entity {
    @SerializedName("albumId")
    public final int ALBUM_ID;
    @SerializedName("id")
    public final int ID;
    @SerializedName("title")
    public final String TITLE;
    @SerializedName("url")
    public final String URL;
    @SerializedName("thumbnailUrl")
    public final String THUMBNAIL_URL;

    public Photo(int album_id, int id, String title, String url, String thumbnail_url) {
        ALBUM_ID = album_id;
        ID = id;
        TITLE = title;
        URL = url;
        THUMBNAIL_URL = thumbnail_url;
    }

    @Override
    public String toString() {
        return "ID: " + ID + ", TITLE: " + TITLE + ", URL: " + URL + ", THUMBNAIL_URL: " + THUMBNAIL_URL;
    }
}
