package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Comment extends Entity {
    @SerializedName("postId")
    public final int POST_ID;
    @SerializedName("id")
    public final int ID;
    @SerializedName("name")
    public final String NAME;
    @SerializedName("email")
    public final String EMAIL;
    @SerializedName("body")
    public final String BODY;

    public Comment(int postId, int id, String name, String email, String body) {
        POST_ID = postId;
        ID = id;
        NAME = name;
        EMAIL = email;
        BODY = body;
    }

    @Override
    public String toString() {
        return "ID: " + ID + ", POST_ID: " + POST_ID + ", NAME: " + NAME + ", EMAIL: " + EMAIL + ", BODY: " + BODY;
    }
}
