package com.epam.webservices.ui.impl;

import com.epam.webservices.dto.*;
import com.epam.webservices.entity.Album;
import com.epam.webservices.entity.Post;
import com.epam.webservices.manager.IDataManager;
import com.epam.webservices.ui.IUserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ConsoleUserInterface implements IUserInterface {
    private final Logger logger = LogManager.getLogger(ConsoleUserInterface.class);

    private final IDataManager messages;
    private final IUserDto userDto;
    private final ITodoDto todoDto;
    private final IPhotoDto photoDto;
    private final IAlbumDto albumDto;
    private final IPostDto postDto;
    private final ICommentDto commentDto;

    private Stage currentStage = Stage.WATCHING_MAIN_MENU;
    private int userId;
    private int albumId;
    private int postId;

    public ConsoleUserInterface(IDataManager messages, IUserDto userDto, ITodoDto todoDto, IPhotoDto photoDto, IAlbumDto albumDto, IPostDto postDto, ICommentDto commentDto) {
        this.messages = messages;
        this.userDto = userDto;
        this.todoDto = todoDto;
        this.photoDto = photoDto;
        this.albumDto = albumDto;
        this.postDto = postDto;
        this.commentDto = commentDto;
    }

    @Override
    public void delegate() {
        displayMessage(messages.getString("main.menu"));
        Scanner scanner = new Scanner(System.in);
        while (true) {
            handleUserInput(scanner.nextLine());
            logger.debug("Stage: " + currentStage);
        }
    }

    private void handleUserInput(String userInput) {
        String response = "";
        switch (currentStage) {
            case WATCHING_MAIN_MENU: //if user in main menu now
                response = messageOnMainMenu(userInput);
                break;
            case GETTING_USER_BY_ID: //if user on search user by id page now
                response = messageOnGettingUserById(userInput);
                break;
            case WATCHING_USER_ACTION_PAGE: //if user choosing user on choosing user action page now
                response = messageOnChoosingUserAction(userInput);
                break;
            case WATCHING_USER_ALBUMS: //if user on page with albums now
                response = messageOnChoosingAlbumId(userInput);
                break;
            case WATCHING_USER_POSTS: //if user on page with posts now
                response = messageOnChoosingPostId(userInput);
                break;
            case WATCHING_ALBUM_MENU: //if user on page with album options now
                response = messageFromAlbumMenu(userInput);
                break;
            case WATCHING_POST_MENU: //if user on page with post options now
                response = messageFromPostMenu(userInput);
                break;
            case PRESSING_ENTER_TO_SHOW_MAIN_MENU:
                response = messageOnPressEnterToShowMainMenu();
                break;
            case PRESSING_ENTER_TO_SHOW_USER_MENU:
                response = messageOnPressEnterToShowUserMenu();
                break;
            case PRESSING_ENTER_TO_SHOW_ALBUM_MENU:
                response = messageOnPressingEnterToShowAlbumMenu();
                break;
            case PRESSING_ENTER_TO_SHOW_POST_MENU:
                response = messageOnPressingEnterToShowPostMenu();
                break;
        }
        displayMessage(response);
    }


    // main menu:

    private String messageOnMainMenu(String input) {
        String response = "";
        switch (toPositiveNumber(input)) {
            case 0: //exit
                System.exit(0);// TODO попробовать сделать более нормально
                break;
            case 1: //get user by id
                response = messageOnGettingUserById();
                break;
            case 2: //get all users
                response = messageOnGettingAllUsers();
                break;
            case 3:
                response = messageOnGettingAllPosts();
                break;
            case 4:
                response = messageOnGettingAllComments();
                break;
            default:
                response = messageOnInvalidInputInMainMenu();
                break;
        }
        return response;
    }


    private String messageOnInvalidInputInMainMenu() {
        return messages.getString("main.menu.invalid.input");
    }

    private String messageOnGettingUserById() {
        currentStage = Stage.GETTING_USER_BY_ID;
        return messages.getString("main.menu.choice.get.user.by.id");

    }

    private String messageOnGettingAllUsers() {
        var response = new StringBuilder(messages.getString("get.all.users.chose"));
        userDto.getUsers().forEach(user -> response.append(user).append('\n'));
        response.append(messageOnUserShouldPressEnterToDisplayMainMenu());
        return response.toString();
    }

    private String messageOnGettingAllPosts() {
        var response = new StringBuilder(messages.getString("get.all.posts.chose"));
        postDto.getPosts().forEach(post -> response.append(post).append('\n'));
        response.append(messageOnUserShouldPressEnterToDisplayMainMenu());
        return response.toString();
    }

    private String messageOnGettingAllComments() {
        var response = new StringBuilder(messages.getString("get.all.comments.chose"));
        commentDto.getComments().forEach(comment -> response.append(comment).append('\n'));
        response.append(messageOnUserShouldPressEnterToDisplayMainMenu());
        return response.toString();
    }

    private String messageOnUserShouldPressEnterToDisplayMainMenu() {
        currentStage = Stage.PRESSING_ENTER_TO_SHOW_MAIN_MENU;
        return messages.getString("press.enter.to.go.to.main.menu");
    }

    private String messageOnPressEnterToShowMainMenu() {
        currentStage = Stage.WATCHING_MAIN_MENU;
        return messages.getString("main.menu");
    }

    // :main menu end


    // choosing user action page:

    private String messageOnChoosingUserAction(String input) {
        int choice = toPositiveNumber(input);
        String response;
        switch (choice) {
            case 0:
                response = messageOnGoToMainMenu();
                break;
            case 1: //search another user menu item
                response = messageOnSearchAnotherUser();
                break;
            case 2: //get user albums menu item
                response = messageOnGetUsersAlbums();
                break;
            case 3: //get user posts menu item
                response = messageOnGetUsersPosts();
                break;
            case 4: //get user todos menu item
                response = messageOnGetUsersTodos();
                break;
            default: //user chose smth invalid
                response = messageOnInvalidInputInUserMenu();
                break;
        }
        return response;
    }

    private String messageOnGoToMainMenu() {
        currentStage = Stage.WATCHING_MAIN_MENU;
        return messages.getString("main.menu");
    }

    private String messageOnSearchAnotherUser() {
        currentStage = Stage.GETTING_USER_BY_ID;
        return messages.getString("choosing.user.action.chose.search.another.user");
    }

    private String messageOnGetUsersAlbums() {
        currentStage = Stage.WATCHING_USER_ALBUMS;
        var response = new StringBuilder(messages.getString("choosing.user.action.chose.get.user.albums.header"));
        albumDto.getAlbumsByUserId(userId).forEach(album -> response.append(album).append('\n'));
        return response.append(messages.getString("choosing.user.action.chose.get.user.albums.footer")).toString();
    }

    private String messageOnGettingUserById(String input) {
        userId = toPositiveNumber(input);
        if (userId == -1) {
            return messages.getString("get.user.by.id.invalid.input");
        } else if (userId == 0) {
            currentStage = Stage.WATCHING_MAIN_MENU;
            return messages.getString("main.menu");
        }
        var user = userDto.getUserById(userId);
        if (user == null) return messages.getString("get.user.by.id.not.found");
        currentStage = Stage.WATCHING_USER_ACTION_PAGE;
        return messages.getString("get.user.by.id.success").replaceAll("%user%", user.toString()).concat(messages.getString("user.menu"));
    }

    private String messageOnGetUsersPosts() {
        currentStage = Stage.WATCHING_USER_POSTS;
        var response = new StringBuilder(messages.getString("choosing.user.action.chose.get.user.posts.header"));
        postDto.getPostsByUserId(userId).forEach(post -> response.append(post).append('\n'));
        return response.append(messages.getString("choosing.user.action.chose.get.user.posts.footer")).toString();
    }

    private String messageOnGetUsersTodos() {
        currentStage = Stage.PRESSING_ENTER_TO_SHOW_USER_MENU;
        var response = new StringBuilder(messages.getString("choosing.user.action.chose.get.user.todos"));
        var todos = todoDto.getTodosByUserId(userId);
        todos.forEach(todo -> response.append(todo).append('\n'));
        response.append(messages.getString("press.enter.to.go.to.user.menu"));
        return response.toString();
    }

    private String messageOnInvalidInputInUserMenu() {
        return messages.getString("choosing.user.action.invalid");
    }

    private String messageOnPressEnterToShowUserMenu() {
        currentStage = Stage.WATCHING_USER_ACTION_PAGE;
        return messages.getString("user.menu");
    }

    // :choosing user action page end


    // choosing album action page:

    private String messageOnChoosingAlbumId(String input) {
        int choice = toPositiveNumber(input);
        if (choice == -1) {
            return messages.getString("choosing.album.id.invalid");
        } else if (choice == 0) {
            currentStage = Stage.WATCHING_USER_ACTION_PAGE;
            return messages.getString("user.menu");
        }
        for (Album album : albumDto.getAlbumsByUserId(userId)) {
            if (choice == album.ID) {
                albumId = album.ID;
                currentStage = Stage.WATCHING_ALBUM_MENU;
                return messages.getString("album.menu");
            }
        }
        return messages.getString("choosing.album.id.not.exist");
    }

    private String messageFromAlbumMenu(String input) {
        String response = "";
        switch (toPositiveNumber(input)) {
            case 0:
                response = messageOnGoToUserMenuFromAlbumMenu();
                break;
            case 1:
                response = messageOnGettingPhotosFromAlbum();
                break;
            default:
                response = messageOnInvalidInputInAlbumMenu();
                break;
        }
        return response;
    }

    private String messageOnGoToUserMenuFromAlbumMenu() {
        currentStage = Stage.WATCHING_USER_ACTION_PAGE;
        return messages.getString("user.menu");
    }

    private String messageOnGettingPhotosFromAlbum() {
        var response = new StringBuilder(messages.getString("choosing.album.action.chose.album.photos.header"));
        photoDto.getPhotosByAlbumId(albumId).forEach(photo -> response.append(photo).append('\n'));
        currentStage = Stage.PRESSING_ENTER_TO_SHOW_ALBUM_MENU;
        return response.append(messages.getString("press.enter.to.go.to.album.menu")).toString();
    }

    private String messageOnInvalidInputInAlbumMenu() {
        return messages.getString("choosing.album.action.invalid");
    }

    private String messageOnPressingEnterToShowAlbumMenu() {
        currentStage = Stage.WATCHING_ALBUM_MENU;
        return messages.getString("album.menu");
    }

    // :choosing album action page end


    // choosing post action page:

    private String messageOnChoosingPostId(String input) {
        int choice = toPositiveNumber(input);
        if (choice == -1) {
            return messages.getString("choosing.post.id.invalid");
        } else if (choice == 0) {
            currentStage = Stage.WATCHING_USER_ACTION_PAGE;
            return messages.getString("user.menu");
        }
        for (Post post : postDto.getPostsByUserId(userId)) {
            if (choice == post.ID) {
                postId = choice;
                currentStage = Stage.WATCHING_POST_MENU;
                return messages.getString("post.menu");
            }
        }
        return messages.getString("choosing.post.id.not.exist");
    }

    private String messageFromPostMenu(String input) {
        String response = "";
        switch (toPositiveNumber(input)) {
            case 0:
                response = messageOnGoToUserMenuFromPostMenu();
                break;
            case 1:
                response = messageOnGettingCommentsOfPost();
                break;
            default:
                response = messageOnInvalidInputInPostMenu();
                break;
        }
        return response;
    }

    private String messageOnGoToUserMenuFromPostMenu() {
        currentStage = Stage.WATCHING_USER_ACTION_PAGE;
        return messages.getString("user.menu");
    }

    private String messageOnGettingCommentsOfPost() {
        var response = new StringBuilder(messages.getString("choosing.post.action.chose.post.comments.header"));
        commentDto.getCommentsByPostId(postId).forEach(comment -> response.append(comment).append('\n'));
        currentStage = Stage.PRESSING_ENTER_TO_SHOW_POST_MENU;
        return response.append("press.enter.to.go.to.post.menu").toString();
    }

    private String messageOnInvalidInputInPostMenu() {
        return messages.getString("choosing.post.action.invalid");
    }

    private String messageOnPressingEnterToShowPostMenu() {
        currentStage = Stage.WATCHING_POST_MENU;
        return messages.getString("post.menu");
    }


    // :choosing post action page end


    // utils:

    private void displayMessage(String message) {
        logger.info("============================================================");
        var rows = message.split("\n");
        for (String row : rows) {
            logger.info(row);
        }
        logger.info("============================================================");
    }

    /**
     * Casts string to number. If input is not a number OR number is smaller than 0 return value will be -1
     *
     * @param str String that should be casted to int
     * @return Integer that was casted from input string
     */
    private int toPositiveNumber(String str) {
        try {
            int result = Integer.parseInt(str);
            return result < 0 ? -1 : result;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    // :utils end

    private enum Stage {
        WATCHING_MAIN_MENU,
        GETTING_USER_BY_ID,
        WATCHING_USER_ACTION_PAGE,
        WATCHING_USER_ALBUMS,
        WATCHING_ALBUM_MENU,
        WATCHING_USER_POSTS,
        WATCHING_POST_MENU,
        PRESSING_ENTER_TO_SHOW_MAIN_MENU,
        PRESSING_ENTER_TO_SHOW_USER_MENU,
        PRESSING_ENTER_TO_SHOW_ALBUM_MENU,
        PRESSING_ENTER_TO_SHOW_POST_MENU
    }
}
