package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Album extends Entity {
    @SerializedName("userId")
    public final int USER_ID;
    @SerializedName("id")
    public final int ID;
    @SerializedName("title")
    public final String TITLE;

    public Album(int user_id, int id, String title) {
        USER_ID = user_id;
        ID = id;
        TITLE = title;
    }

    @Override
    public String toString() {
        return "ID: " + ID + ", TITLE: " + TITLE;
    }
}
