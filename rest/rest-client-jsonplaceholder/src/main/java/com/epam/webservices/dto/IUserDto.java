package com.epam.webservices.dto;

import com.epam.webservices.entity.User;

import java.util.List;

public interface IUserDto {
    User getUserById(int userId);

    List<User> getUsers();
}
