package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.IPostDto;
import com.epam.webservices.entity.Post;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class PostDto implements IPostDto {
    private final IDataManager config;

    public PostDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public List<Post> getPosts() {
        var link = config.getString("jsonplaceholder.posts.link");
        return Unirest.get(link).asObject(new GenericType<List<Post>>() {}).getBody();
    }

    @Override
    public Post getPostById(int id) {
        var link = config.getString("jsonplaceholder.posts.link");
        var result = Unirest.get(link).queryString("id", id).asObject(new GenericType<List<Post>>() {}).getBody();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public List<Post> getPostsByUserId(int userId) {
        var link = config.getString("jsonplaceholder.posts.link");
        return Unirest.get(link).queryString("userId", userId).asObject(new GenericType<List<Post>>() {}).getBody();
    }
}
