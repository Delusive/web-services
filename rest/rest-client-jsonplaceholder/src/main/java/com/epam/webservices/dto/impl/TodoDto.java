package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.ITodoDto;
import com.epam.webservices.entity.Todo;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class TodoDto implements ITodoDto {
    private final IDataManager config;

    public TodoDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public List<Todo> getTodosByUserId(int userId) {
        var link = config.getString("jsonplaceholder.todos.link");
        return Unirest.get(link).queryString("userId", userId).asObject(new GenericType<List<Todo>>() {}).getBody();
    }
}
