package com.epam.webservices.manager;

public interface IDataManager {
    String getString(String paramName);

    int getInt(String paramName);
}