package com.epam.webservices.dto.impl;

import com.epam.webservices.dto.IAlbumDto;
import com.epam.webservices.entity.Album;
import com.epam.webservices.manager.IDataManager;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class AlbumDto implements IAlbumDto {
    private final IDataManager config;

    public AlbumDto(IDataManager config) {
        this.config = config;
    }

    @Override
    public List<Album> getAlbumsByUserId(int userId) {
        var link = config.getString("jsonplaceholder.albums.link");
        return Unirest.get(link).queryString("userId", userId).asObject(new GenericType<List<Album>>() {}).getBody();
    }
}
