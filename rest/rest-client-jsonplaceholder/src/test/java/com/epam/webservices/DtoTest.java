package com.epam.webservices;


import com.epam.webservices.dto.*;
import com.epam.webservices.dto.impl.*;
import com.epam.webservices.manager.IDataManager;
import com.epam.webservices.manager.impl.PropertiesManager;
import kong.unirest.GsonObjectMapper;
import kong.unirest.Unirest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DtoTest {
    private final IDataManager config = new PropertiesManager("/client.properties");

    @BeforeAll
    static void before() {
        Unirest.config().setObjectMapper(new GsonObjectMapper());
    }

    @Test
    @DisplayName("Got requested post")
    void postDtoTest() {
        IPostDto postDto = new PostDto(config);
        //By post id
        int postId = 1;
        var post = postDto.getPostById(postId);
        assertEquals(postId, post.ID);
        //By user id
        int userId = 3;
        var posts = postDto.getPostsByUserId(userId);
        if (posts.isEmpty()) fail("No posts received!");
        posts.forEach(post1 -> assertEquals(userId, post1.USER_ID));
    }

    @Test
    @DisplayName("Got requested comments")
    void commentDtoTest() {
        ICommentDto commentDto = new CommentDto(config);
        int postId = 4;
        var comments = commentDto.getCommentsByPostId(postId);
        if (comments.isEmpty()) fail("No comments received!");
        comments.forEach(comment -> assertEquals(postId, comment.POST_ID));
    }

    @Test
    @DisplayName("Go requested albums")
    void albumDtoTest() {
        IAlbumDto albumDto = new AlbumDto(config);
        int userId = 4;
        var albums = albumDto.getAlbumsByUserId(userId);
        if (albums.isEmpty()) fail("No albums received!");
        albums.forEach(album -> assertEquals(userId, album.USER_ID));
    }

    @Test
    @DisplayName("Got requested photos")
    void photoDtoTest() {
        IPhotoDto photoDto = new PhotoDto(config);
        int albumId = 4;
        var photos = photoDto.getPhotosByAlbumId(albumId);
        if (photos.isEmpty()) fail("No photos received!");
        photos.forEach(photo -> assertEquals(albumId, photo.ALBUM_ID));
    }

    @Test
    @DisplayName("Got requested user")
    void userDtoTest() {
        IUserDto userDto = new UserDto(config);
        int userId = 3;
        var user = userDto.getUserById(userId);
        assertEquals(userId, user.ID);
    }

    @Test
    @DisplayName("Got requested todo")
    void todoDtoTest() {
        ITodoDto todoDto = new TodoDto(config);
        int userId = 2;
        var todos = todoDto.getTodosByUserId(userId);
        if (todos.isEmpty()) fail("No todos received!");
        todos.forEach(todo -> assertEquals(userId, todo.USER_ID));
    }

}
