package com.epam.jdbc;

import com.auth0.jwt.JWT;
import com.epam.jdbc.ui.IUserInterface;
import com.epam.jdbc.ui.UserInterface;

public class Main {
    private static IUserInterface userInterface = new UserInterface();

    public static void main(String[] args) {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoxLCJpc3MiOiJsaWJyYXJ5IiwiSUQiOjEsImxvZ2luIjoiYWRtaW4ifQ.R3Ylm_dDOM55kFRSaaoHrImf_a_mfWYOnAS0w8s_QG8";
        JWT.decode(token).getClaim("role").asInt();
        JWT.decode(token).getClaim("ID").asInt();
        JWT.decode(token).getClaim("login").asString();
        userInterface.delegate();
    }
}
