-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.11 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `library`;

-- Dumping structure for table library.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `authorId` int(11) NOT NULL AUTO_INCREMENT,
  `authorName` varchar(50) NOT NULL,
  PRIMARY KEY (`authorId`),
  UNIQUE KEY `authorName` (`authorName`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table library.authors: ~11 rows (approximately)
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`authorId`, `authorName`) VALUES
	(12, '1'),
	(8, 'Bruce Eckel'),
	(4, 'One'),
	(5, 'Айя К.'),
	(9, 'Герберт Шилдт'),
	(11, 'ДЖейсон Стетем'),
	(10, 'Лев Тигр'),
	(6, 'Масаши Кишимото'),
	(3, 'Мурата Юсукэ'),
	(2, 'Такэмура Михаил'),
	(7, 'Хадзимэ Исаяма');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

-- Dumping structure for table library.bookmarks
CREATE TABLE IF NOT EXISTS `bookmarks` (
  `bookmarkId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `bookId` bigint(20) NOT NULL DEFAULT '0',
  `page` int(11) NOT NULL,
  PRIMARY KEY (`bookmarkId`),
  KEY `user` (`userId`),
  KEY `book` (`bookId`),
  CONSTRAINT `book` FOREIGN KEY (`bookId`) REFERENCES `books` (`ISBN`),
  CONSTRAINT `user` FOREIGN KEY (`userId`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table library.bookmarks: ~3 rows (approximately)
/*!40000 ALTER TABLE `bookmarks` DISABLE KEYS */;
INSERT INTO `bookmarks` (`bookmarkId`, `userId`, `bookId`, `page`) VALUES
	(1, 1, 9785699315024, 10),
	(2, 1, 131872486, 12),
	(3, 1, 131872486, 1200);
/*!40000 ALTER TABLE `bookmarks` ENABLE KEYS */;

-- Dumping structure for table library.books
CREATE TABLE IF NOT EXISTS `books` (
  `ISBN` bigint(20) NOT NULL DEFAULT '0',
  `bookName` varchar(50) NOT NULL DEFAULT '',
  `authorId` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `isActive` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`ISBN`),
  KEY `authorId` (`authorId`),
  CONSTRAINT `authorId` FOREIGN KEY (`authorId`) REFERENCES `authors` (`authorid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table library.books: ~13 rows (approximately)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` (`ISBN`, `bookName`, `authorId`, `date`, `isActive`) VALUES
	(1, '', 12, '2019-01-01', 1),
	(123213, '\\\\t\\s', 10, '2015-01-01', 1),
	(131872486, 'Thinking in Java', 8, '2006-01-01', 1),
	(9785389084230, 'Манга Атака титанов. Книга 1', 7, '2016-02-04', 1),
	(9785389141117, 'One-Punch Man. Книги 1-2', 3, '2018-01-01', 1),
	(9785389144385, 'One-Punch Man. Книга 2', 3, '2018-01-01', 1),
	(9785389148857, 'One-Punch Man. Книга 3', 4, '2018-01-01', 1),
	(9785389155459, 'One-Punch Man Книга 4', 4, '2019-01-01', 1),
	(9785699315024, 'Наруто. Книга 1. Наруто Узумаки', 6, '2010-01-01', 1),
	(9785919960812, 'Манга. Восхождение героя Щита. Том 1', 5, '2016-01-01', 1),
	(9785919960966, 'Восхождение героя Щита. Том 3', 5, '2016-01-01', 1),
	(9785970603550, 'Занимательная биохимия', 2, '2016-01-01', 1),
	(9785979001272, 'С++ для начинающих. Шаг за шагом', 9, '2013-01-01', 1);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;

-- Dumping structure for table library.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `roleId` int(20) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(50) NOT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table library.roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`roleId`, `rolename`) VALUES
	(1, 'BASE'),
	(2, 'ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table library.users
CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `roleId` int(20) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `username` (`username`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `roleId` FOREIGN KEY (`roleId`) REFERENCES `roles` (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table library.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`userId`, `username`, `password`, `roleId`) VALUES
	(1, 'Supeser', '0B5ECC7D210806A5A95531C4E0204B25', 2),
	(7, 'Kakashi', 'c055820999766963589bff2ca3006b6e', 2),
	(8, 'Ayanokoji', '202cb962ac59075b964b07152d234b70', 2),
	(9, 'левтигр', 'e10adc3949ba59abbe56e057f20f883e', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
