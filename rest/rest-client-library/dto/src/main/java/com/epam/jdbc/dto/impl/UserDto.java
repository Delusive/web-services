package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.PropertiesManager;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserDto implements IUserDto {
    private final PropertiesManager config;
    private final String url;
    private final String tokenParamName;

    public UserDto(PropertiesManager config) {
        this.config = config;
        url = config.getString("rest.user.url");
        tokenParamName = config.getString("rest.token.param.name");
    }

    @Override
    public User getUserById(String token, int id) {
        String userIdParamName = config.getString("rest.user.id.param.name");
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(userIdParamName, id)
                .asJson();
        return toNormalUser(response.getBody().getObject());
    }

    @Override
    public boolean addUserToDatabase(String token, User user) {
        String loginParamName = config.getString("rest.user.login.param.name");
        String passwordParamName = config.getString("rest.user.password.param.name");
        String roleIdParamName = config.getString("rest.user.role.param.name");
        HttpResponse response = Unirest.post(url)
                .queryString(tokenParamName, token)
                .queryString(loginParamName, user.NAME)
                .queryString(passwordParamName, user.PASSWORD_HASH)
                .queryString(roleIdParamName, user.ROLE == User.Role.USER ? 2 : 1)
                .asEmpty();
        return response.isSuccess();
    }

    @Override
    public List<User> getUsersByNamePattern(String token, String namePattern) {
        List<User> result = new ArrayList<>();
        String loginParamName = config.getString("rest.user.login.param.name");
        Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(loginParamName, namePattern)
                .asJson().getBody().getArray()
                .forEach(obj -> result.add(toNormalUser((JSONObject) obj)));
        return result;
    }

    @Override
    public boolean changeUserRole(String token, User user, User.Role toRole) {
        String roleIdParamName = config.getString("rest.user.role.id.param.name");
        String userIdParamName = config.getString("rest.user.user.id.param.name");
        HttpResponse response = Unirest.put(url)
                .queryString(tokenParamName, token)
                .queryString(roleIdParamName, toRole == User.Role.ADMIN ? 1 : 2)
                .queryString(userIdParamName, user.ID)
                .asEmpty();
        return response.isSuccess();
    }

    @Override
    public String authenticate(String username, String password) {
        String url = config.getString("rest.auth.url");
        String loginParam = config.getString("rest.auth.login.param");
        String passwordParam = config.getString("rest.auth.password.param");
        JSONObject response = Unirest.post(url)
                .queryString(loginParam, username)
                .queryString(passwordParam, password)
                .asJson().getBody().getObject();
        return response.getBoolean("ok") ? response.getString("JWT") : null;
    }

    private User toNormalUser(JSONObject object) {
        int id = object.getInt("id");
        String name = object.getString("login");
        int roleId = object.getInt("role");
        User.Role role = roleId == 2 ? User.Role.ADMIN : User.Role.USER;
        return new User(id, name, role);
    }
}
