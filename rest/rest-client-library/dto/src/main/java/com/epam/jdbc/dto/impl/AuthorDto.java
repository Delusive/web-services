package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IAuthorDto;
import com.epam.jdbc.entity.Author;
import com.epam.jdbc.mics.PropertiesManager;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.util.List;

public class AuthorDto implements IAuthorDto {
    private final PropertiesManager config;

    private final String url;
    private final String tokenParamName;


    public AuthorDto(PropertiesManager config) {
        this.config = config;
        url = config.getString("rest.author.url");
        tokenParamName = config.getString("rest.token.param.name");
    }

    @Override
    public List<Author> getAuthors(String token) {
        return Unirest.get(url)
                .queryString(tokenParamName, token)
                .asObject(new GenericType<List<Author>>() {
                })
                .getBody();
    }

    @Override
    public Author getAuthorById(String token, int id) {
        String authorIdParamName = config.getString("rest.author.id.param");
        return Unirest.get(url)
                .queryString(authorIdParamName, String.valueOf(id))
                .queryString(tokenParamName, token)
                .asObject(Author.class)
                .getBody();
    }

    @Override
    public List<Author> getAuthorsByName(String token, String namePattern) {
        String nameParam = config.getString("rest.author.name.param");
        return Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(nameParam, namePattern)
                .asObject(new GenericType<List<Author>>() {
                })
                .getBody();
    }

    @Override
    public Author getAuthorByName(String token, String name) {
        List<Author> authors = getAuthorsByName(token, name);
        if (authors == null) return null;
        for (Author author : authors) {
            if (author.NAME.equalsIgnoreCase(name)) return author;
        }
        return null;
    }

    @Override
    public Author getOrCreateAuthor(String token, String name) {
        Author author = getAuthorByName(token, name);
        if (author == null) { // doesn't exist
            String nameParamName = config.getString("rest.author.name.param");
            HttpResponse response = Unirest.post(url)
                    .queryString(tokenParamName, token)
                    .queryString(nameParamName, name)
                    .asEmpty();
            if (!response.isSuccess()) return null;
            return getAuthorByName(token, name);
        }
        return author;
    }
}
