package com.epam.jdbc.dto;

import com.epam.jdbc.entity.User;

import java.util.List;

public interface IUserDto extends IBaseDto {
    User getUserById(String token, int id);

    boolean addUserToDatabase(String token, User user);

    List<User> getUsersByNamePattern(String token, String namePattern);

    boolean changeUserRole(String token, User user, User.Role toRole);

    String authenticate(String username, String password);
}
