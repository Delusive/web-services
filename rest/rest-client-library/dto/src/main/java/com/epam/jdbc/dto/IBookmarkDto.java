package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;

import java.util.List;

public interface IBookmarkDto extends IBaseDto {
    List<Bookmark> getUserBookmarks(String token, User user);

    boolean deleteBookmarkById(String token, int id);

    boolean addBookmark(String token, User user, Book book, int page);
}
