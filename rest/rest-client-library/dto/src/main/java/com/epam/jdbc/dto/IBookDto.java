package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Book;

import java.util.List;

public interface IBookDto extends IBaseDto {

    Book getBookByISBN(String token, long isbn);

    List<Book> getBooksByAuthorId(String token, int authorId);

    List<Book> getBooksByName(String token, String bookNamePattern);

    List<Book> getBooksByDateInterval(String token, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByAuthorIdAndBookName(String token, int authorId, String bookName);

    List<Book> getBooksByAuthorIdAndDateInterval(String token, int authorId, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndDateInterval(String token, String bookNamePattern, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndAuthorIdAndDateInterval(String token, String bookName, int authorId, long firstTimestamp, long secondTimestamp);

    boolean deleteBookByIsbn(String token, long isbn);

    boolean addBookToDatabase(String token, Book book);

}
