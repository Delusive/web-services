package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Author;

import java.util.List;

public interface IAuthorDto extends IBaseDto {
    List<Author> getAuthors(String token);

    Author getAuthorById(String token, int id);

    List<Author> getAuthorsByName(String token, String namePattern);

    Author getAuthorByName(String token, String name);

    Author getOrCreateAuthor(String token, String name);
}
