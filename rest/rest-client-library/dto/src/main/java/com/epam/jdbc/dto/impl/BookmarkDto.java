package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IBookDto;
import com.epam.jdbc.dto.IBookmarkDto;
import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.PropertiesManager;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BookmarkDto implements IBookmarkDto {
    private final IBookDto bookDto;
    private final IUserDto userDto;
    private final PropertiesManager config;
    private final String url;
    private final String tokenParamName;

    public BookmarkDto(PropertiesManager config, IBookDto bookDto, IUserDto userDto) {
        this.bookDto = bookDto;
        this.userDto = userDto;
        this.config = config;
        url = config.getString("rest.bookmark.url");
        tokenParamName = config.getString("rest.token.param.name");
    }

    @Override
    public List<Bookmark> getUserBookmarks(String token, User user) {
        List<Bookmark> bookmarks = new ArrayList<>();
        String userIdParamName = config.getString("rest.bookmark.user.id.param.name");
        Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(userIdParamName, user.ID)
                .asJson().getBody().getArray()
                .forEach(obj -> bookmarks.add(toNormalBookmark(token, (JSONObject) obj)));
        return bookmarks;
    }


    @Override
    public boolean deleteBookmarkById(String token, int id) {
        String idParamName = config.getString("rest.bookmark.id.param.name");
        return Unirest.delete(url)
                .queryString(tokenParamName, token)
                .queryString(idParamName, id)
                .asEmpty()
                .isSuccess();
    }

    @Override
    public boolean addBookmark(String token, User user, Book book, int page) {
        String bookIdParamName = config.getString("rest.bookmark.book.id.param.name");
        String pageParamName = config.getString("rest.bookmark.page.param.name");
        return Unirest.post(url)
                .queryString(tokenParamName, token)
                .queryString(bookIdParamName, book.ISBN)
                .queryString(pageParamName, page)
                .asEmpty()
                .isSuccess();
    }

    private Bookmark toNormalBookmark(String token, JSONObject jsonObject) {
        int bookmarkId = jsonObject.getInt("id");
        int bookId = jsonObject.getInt("bookId");
        int userId = jsonObject.getInt("userId");
        int page = jsonObject.getInt("page");
        Book book = bookDto.getBookByISBN(token, bookId);
        User user = userDto.getUserById(token, userId);

        return new Bookmark(bookmarkId, user, book, page);
    }
}
