package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IAuthorDto;
import com.epam.jdbc.dto.IBookDto;
import com.epam.jdbc.entity.Author;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.mics.PropertiesManager;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class BookDto implements IBookDto {
    private final PropertiesManager config;
    private final IAuthorDto authorDto;

    private final String url;
    private final String tokenParamName;

    public BookDto(PropertiesManager config, IAuthorDto authorDto) {
        this.config = config;
        this.authorDto = authorDto;
        url = config.getString("rest.book.url");
        tokenParamName = config.getString("rest.token.param.name");
    }


    @Override
    public Book getBookByISBN(String token, long isbn) {
        String idParamName = config.getString("rest.book.id.param.name");
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(idParamName, isbn)
                .asJson();
        return response.getBody() == null ? null : toNormalBook(token, response.getBody().getObject());
    }

    @Override
    public List<Book> getBooksByAuthorId(String token, int authorId) {
        List<Book> books = new ArrayList<>();
        String authorNameParamName = config.getString("rest.book.author.name.param.name");
        Author author = authorDto.getAuthorById(token, authorId);
        Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(authorNameParamName, author.NAME)
                .asJson().getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByName(String token, String bookNamePattern) {
        List<Book> books = new ArrayList<>();
        String bookNameParamName = config.getString("rest.book.name.param.name");
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(bookNameParamName, bookNamePattern)
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByDateInterval(String token, long firstTimestamp, long secondTimestamp) {
        List<Book> books = new ArrayList<>();
        String firstDateParamName = config.getString("rest.book.first.date.param.name");
        String secondDateParamName = config.getString("rest.book.second.date.param.name");
        Date first = new Date(firstTimestamp);
        Date second = new Date(secondTimestamp);
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(firstDateParamName, first.toString())
                .queryString(secondDateParamName, second.toString())
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByAuthorIdAndBookName(String token, int authorId, String bookName) {
        List<Book> books = new ArrayList<>();
        String authorNameParamName = config.getString("rest.book.author.name.param.name");
        String bookNameParamName = config.getString("rest.book.name.param.name");
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(authorNameParamName, authorDto.getAuthorById(token, authorId).ID)
                .queryString(bookNameParamName, bookName)
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByAuthorIdAndDateInterval(String token, int authorId, long firstTimestamp, long secondTimestamp) {
        List<Book> books = new ArrayList<>();
        String authorNameParamName = config.getString("rest.book.author.name.param.name");
        String firstDateParamName = config.getString("rest.book.first.date.param.name");
        String secondDateParamName = config.getString("rest.book.second.date.param.name");
        Date first = new Date(firstTimestamp);
        Date second = new Date(secondTimestamp);
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(firstDateParamName, first.toString())
                .queryString(secondDateParamName, second.toString())
                .queryString(authorNameParamName, authorDto.getAuthorById(token, authorId).ID)
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByNameAndDateInterval(String token, String bookNamePattern, long firstTimestamp, long secondTimestamp) {
        List<Book> books = new ArrayList<>();
        String bookNameParamName = config.getString("rest.book.name.param.name");
        String firstDateParamName = config.getString("rest.book.first.date.param.name");
        String secondDateParamName = config.getString("rest.book.second.date.param.name");
        Date first = new Date(firstTimestamp);
        Date second = new Date(secondTimestamp);
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(firstDateParamName, first.toString())
                .queryString(secondDateParamName, second.toString())
                .queryString(bookNameParamName, bookNamePattern)
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public List<Book> getBooksByNameAndAuthorIdAndDateInterval(String token, String bookNamePattern, int authorId, long firstTimestamp, long secondTimestamp) {
        List<Book> books = new ArrayList<>();
        String authorNameParamName = config.getString("rest.book.author.name.param.name");
        String bookNameParamName = config.getString("rest.book.name.param.name");
        String firstDateParamName = config.getString("rest.book.first.date.param.name");
        String secondDateParamName = config.getString("rest.book.second.date.param.name");
        Date first = new Date(firstTimestamp);
        Date second = new Date(secondTimestamp);
        HttpResponse<JsonNode> response = Unirest.get(url)
                .queryString(tokenParamName, token)
                .queryString(firstDateParamName, first.toString())
                .queryString(secondDateParamName, second.toString())
                .queryString(authorNameParamName, authorDto.getAuthorById(token, authorId).ID)
                .queryString(bookNameParamName, bookNamePattern)
                .asJson();
        response.getBody().getArray().forEach(obj -> books.add(toNormalBook(token, (JSONObject) obj)));
        return books;
    }

    @Override
    public boolean deleteBookByIsbn(String token, long isbn) {
        String idBookParamName = config.getString("rest.book.id.param.name");
        HttpResponse response = Unirest.delete(url)
                .queryString(tokenParamName, token)
                .queryString(idBookParamName, isbn)
                .asEmpty();
        return response.isSuccess();
    }

    @Override
    public boolean addBookToDatabase(String token, Book book) {
        String bookNameParamName = config.getString("rest.book.name.param.name");
        String authorIdParamName = config.getString("rest.book.author.id.param.name");
        String issueDateParamName = config.getString("rest.book.issue.date.param.name");
        Author author = authorDto.getOrCreateAuthor(token, book.AUTHOR);
        if (author == null) return false;
        HttpResponse response = Unirest.post(url)
                .queryString(tokenParamName, token)
                .queryString(bookNameParamName, book.NAME)
                .queryString(authorIdParamName, author.ID)
                .queryString(issueDateParamName, book.DATE.toString())
                .asEmpty();
        return response.isSuccess();
    }

    private Book toNormalBook(String token, JSONObject jsonObj) {
        long isbn = jsonObj.getLong("ISBN");
        String name = jsonObj.getString("name");
        String author = authorDto.getAuthorById(token, jsonObj.getInt("authorId")).NAME;
        Date date = Date.valueOf(jsonObj.getString("issueYear"));
        return new Book(isbn, name, author, date);
    }

}
