package com.epam.jdbc.entity;

public class Bookmark extends Entity {
    public final int ID;
    public final User USER;
    public final Book BOOK;
    public final int PAGE;

    public Bookmark(int id, User user, Book book, int page) {
        this.ID = id;
        this.USER = user;
        this.BOOK = book;
        this.PAGE = page;
    }

    @Override
    public String toString() {
        return BOOK.toString() + " | Page: " + PAGE;
    }
}
