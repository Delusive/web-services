package com.epam.jdbc.entity;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

public class Book extends Entity {
    @SerializedName("ISBN")
    public final long ISBN;
    @SerializedName("name")
    public final String NAME;
    public final String AUTHOR;
    @SerializedName("issueYear")
    public final Date DATE;

    public Book(long isbn, String name, String author, Date date) {
        this.ISBN = isbn;
        this.NAME = name;
        this.AUTHOR = author;
        this.DATE = date;
    }

    @Override
    public String toString() {
        return String.format("ISBN: %s | Name: %s | Author: %s | Publish Date: %s",
                ISBN,
                NAME,
                AUTHOR,
                DATE);
    }
}
