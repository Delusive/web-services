package com.epam.jdbc.entity;

public class User extends Entity {
    public final int ID;
    public final String NAME;
    public final Role ROLE;
    public final String PASSWORD_HASH;

    public User(int id, String name, Role role, String passwordHash) {
        this.ID = id;
        this.NAME = name;
        this.ROLE = role;
        this.PASSWORD_HASH = passwordHash;
    }

    public User(int ID, String NAME, Role ROLE) {
        this.ID = ID;
        this.NAME = NAME;
        this.ROLE = ROLE;
        PASSWORD_HASH = null;
    }

    public enum Role {
        USER(null),
        ADMIN(USER);

        public final Role PARENT;

        Role(Role parent) {
            this.PARENT = parent;
        }

        public boolean hasParentOrEquals(Role maybeParent) {
            //TODO do it better PLZ
            if (maybeParent == this || maybeParent == this.PARENT) return true;
            if (PARENT == null) return false;
            return PARENT.hasParentOrEquals(maybeParent);
        }

    }
}
