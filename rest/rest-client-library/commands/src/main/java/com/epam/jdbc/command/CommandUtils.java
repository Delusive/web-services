package com.epam.jdbc.command;

import com.epam.jdbc.mics.IDisplayManager;

import java.util.Scanner;

public class CommandUtils {

    /**
     *
     * @return true if str is like yes, false if str like no
     */
    public static boolean yesOrNoDialog(Scanner in, IDisplayManager out) {
        while (true) {
            switch (in.nextLine().toLowerCase()) {
                case "y":
                case "yes":
                case "д":
                case "да":
                    return true;
                case "n":
                case "no":
                case "н":
                case "нет":
                    return false;
                default:
                    out.displayMessage("Try again. (y/n)");
            }
        }
    }

}
