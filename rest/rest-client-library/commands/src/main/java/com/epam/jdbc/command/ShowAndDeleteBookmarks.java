package com.epam.jdbc.command;

import com.epam.jdbc.dto.IBookmarkDto;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ShowAndDeleteBookmarks implements IExecutableCommand {
    private final IDisplayManager DISPLAY_MANAGER;
    private final IBookmarkDto BOOKMARK_DAO;
    private final User USER;
    private final Properties PROPS;

    public ShowAndDeleteBookmarks(IDisplayManager displayManager, IBookmarkDto bookmarkDao, User user, Properties localization) {
        DISPLAY_MANAGER = displayManager;
        BOOKMARK_DAO = bookmarkDao;
        USER = user;
        PROPS = localization;
    }

    @Override
    public void execute(String token) {
        execute(token, BOOKMARK_DAO, USER, DISPLAY_MANAGER);
    }

    private void execute(String token, IBookmarkDto bookmarkDao, User user, IDisplayManager displayManager) {
        List<Bookmark> userBookmarks = bookmarkDao.getUserBookmarks(token, user);
        if (userBookmarks.isEmpty()) {
            displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.nobookmarks"));
            return;
        }
        displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.header"));
        int i = 1;
        for (Bookmark userBookmark : userBookmarks) {
            displayManager.displayMessage(i + ". " + userBookmark.toString());
            i++;
        }
        //Does user want to delete?
        Scanner in = new Scanner(System.in);
        if (userBookmarks.size() == 1) {
            displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.offer.one"));
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.offer.two"));
        }
        if (!CommandUtils.yesOrNoDialog(in, displayManager)) {
            return;
        }
        //User asked "yes". Removing :>
        if (userBookmarks.size() == 1) {
            if (bookmarkDao.deleteBookmarkById(token, userBookmarks.get(0).ID)) {
                displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.success"));
            } else {
                displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.error"));
            }
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.choosebookmark")
                    .replaceAll("%bookmarksCount%", String.valueOf(userBookmarks.size())));
            while (true) {
                try {
                    int bookmarkNum = Integer.parseInt(in.nextLine());
                    if (bookmarkNum < 1 || bookmarkNum > userBookmarks.size()) {
                        displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.bookmarknotfound"));
                        continue;
                    }
                    if (bookmarkDao.deleteBookmarkById(token, userBookmarks.get(bookmarkNum - 1).ID)) {
                        displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.success"));
                    } else {
                        displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.error"));
                    }
                    break;
                } catch (NumberFormatException e) {
                    displayManager.displayMessage(PROPS.getProperty("command.showbookmarks.delete.invalidbookmarknumber"));
                }
            }
        }
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.showbookmarks.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.USER;
    }
}
