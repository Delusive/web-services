package com.epam.jdbc.command;

import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Properties;
import java.util.Scanner;

public class AddNewUser implements IExecutableCommand {
    private final IDisplayManager DISPLAY_MANAGER;
    private final IUserDto USER_DAO;
    private final Properties PROPS;

    public AddNewUser(IDisplayManager displayManager, IUserDto userDao, Properties props) {
        DISPLAY_MANAGER = displayManager;
        USER_DAO = userDao;
        PROPS = props;
    }

    @Override
    public void execute(String token) {
        execute(token, USER_DAO, DISPLAY_MANAGER);
    }

    private void execute(String token, IUserDto userDao, IDisplayManager displayManager) {
        Scanner in = new Scanner(System.in);
        displayManager.displayMessage(PROPS.getProperty("command.adduser.entername"));
        String username = whileEmpty(PROPS.getProperty("command.adduser.emptyname"), in);
        displayManager.displayMessage(PROPS.getProperty("command.adduser.enterpass"));
        String password = whileEmpty(PROPS.getProperty("command.adduser.emptypass"), in);
        User user = new User(0, username, User.Role.USER, DigestUtils.md5Hex(password));
        if(userDao.addUserToDatabase(token, user)) {
            displayManager.displayMessage(PROPS.getProperty("command.adduser.success"));
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.adduser.error"));
        }
    }

    private String whileEmpty(String msgIfEmpty, Scanner in) {
        String result;
        while (true) {
            result = in.nextLine();
            if ("".equals(result)) {
                DISPLAY_MANAGER.displayMessage(msgIfEmpty);
                continue;
            }
            break;
        }
        return result;
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.adduser.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.ADMIN;
    }
}
