package com.epam.jdbc.command;

import com.epam.jdbc.entity.User;

public interface IExecutableCommand {
    void execute(String token);
    String getDescription();

    User.Role getAllowedForRole();
}
