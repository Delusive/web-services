package com.epam.jdbc.mics.exceptions;

public class PropertiesException extends RuntimeException {
    public PropertiesException(String s) {
        super(s);
    }
}
