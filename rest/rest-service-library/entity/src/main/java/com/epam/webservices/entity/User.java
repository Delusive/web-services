package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class User extends Entity {
    @SerializedName("id")
    public final int ID;
    @SerializedName("name")
    public final String NAME;
    @SerializedName("role")
    public final Role ROLE;
    public final transient String PASSWORD_HASH;

    public User(int id, String name, Role role, String passwordHash) {
        this.ID = id;
        this.NAME = name;
        this.ROLE = role;
        this.PASSWORD_HASH = passwordHash;
    }

    public enum Role {
        @SerializedName("BASE")
        BASE(null),
        @SerializedName("ADMIN")
        ADMIN(BASE);

        public final Role PARENT;

        Role(Role parent) {
            this.PARENT = parent;
        }

        public boolean hasParentOrEquals(Role maybeParent) {
            //TODO do it better PLZ
            if (maybeParent == this || maybeParent == this.PARENT) return true;
            if (PARENT == null) return false;
            return PARENT.hasParentOrEquals(maybeParent);
        }

    }
}
