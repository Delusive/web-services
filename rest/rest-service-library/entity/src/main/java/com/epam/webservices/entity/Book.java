package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

public class Book extends Entity {
    @SerializedName("isbn")
    public final long ISBN;
    @SerializedName("name")
    public final String NAME;
    @SerializedName("author")
    public final String AUTHOR;
    @SerializedName("date")
    public final Date DATE;

    public Book(long isbn, String name, String author, Date date) {
        this.ISBN = isbn;
        this.NAME = name;
        this.AUTHOR = author;
        this.DATE = date;
    }

    @Override
    public String toString() {
        return String.format("ISBN: %s | Name: %s | Author: %s | Publish Date: %s",
                ISBN,
                NAME,
                AUTHOR,
                DATE);
    }
}
