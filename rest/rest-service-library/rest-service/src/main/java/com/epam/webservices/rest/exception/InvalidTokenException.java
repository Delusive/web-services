package com.epam.webservices.rest.exception;

public class InvalidTokenException extends RuntimeException {
    public InvalidTokenException(Throwable cause) {
        super(cause);
    }
}
