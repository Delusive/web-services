package com.epam.webservices.rest;

import com.epam.webservices.entity.User;

import java.sql.Date;

public class Utils {
    static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    static boolean isLong(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    static Date toDate(String str) {
        try {
            return Date.valueOf(str);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    static User.Role toRole(String str) {
        try {
            return User.Role.valueOf(str);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
