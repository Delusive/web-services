package com.epam.webservices.rest;

import com.epam.webservices.dao.IAuthorDao;
import com.epam.webservices.dao.IBookDao;
import com.epam.webservices.dao.IBookmarkDao;
import com.epam.webservices.dao.IUserDao;
import com.epam.webservices.entity.Book;
import com.epam.webservices.entity.Bookmark;
import com.epam.webservices.entity.User;
import com.epam.webservices.rest.exception.InvalidTokenException;
import com.google.gson.Gson;
import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class RestService {
    private final Gson gson = new Gson();

    private final String IP;
    private final int PORT;

    private final JwtManager jwtManager;
    private final IAuthorDao authorDao;
    private final IBookDao bookDao;
    private final IBookmarkDao bookmarkDao;
    private final IUserDao userDao;

    public RestService(String ip, int port, JwtManager jwtManager, IAuthorDao authorDao, IBookDao bookDao, IBookmarkDao bookmarkDao, IUserDao userDao) {
        IP = ip;
        PORT = port;
        this.jwtManager = jwtManager;
        this.authorDao = authorDao;
        this.bookDao = bookDao;
        this.bookmarkDao = bookmarkDao;
        this.userDao = userDao;
    }

    public void init() {
        ipAddress(IP);
        port(PORT);

        initFilters();
        initExceptionHandlers();
        initRoutes();
    }

    private void initFilters() {
        before((request, response) -> {
            response.type("text/json");
        });
    }

    private void initExceptionHandlers() {
        notFound((request, response) -> gson.toJson(Map.of("ok", "false", "error", "Not found")));
        exception(InvalidTokenException.class, (e, request, response) -> {
            response.body(gson.toJson(Map.of("ok", "false", "error", e.getMessage())));
            response.status(HttpStatus.UNAUTHORIZED_401);
        });

    }

    private void initRoutes() {
        path("/api", () -> {
            post("/auth", this::authenticate);
            post("/users/:name", this::changeUserRole);

            get("/authors", this::getAuthors);
            get("/books", this::getBooks);
            get("/bookmarks", this::getBookmarks);
            get("/users/:name", this::getUsers);

            put("/bookmarks", this::addBookmark);
            put("/books", this::addBook);
            put("/users", this::addUser);

            delete("/books/:isbn", this::deleteBook);
            delete("/bookmarks/:id", this::deleteBookmark);
        });
    }

    // auth:

    /**
     * Checking specified username and password and if they valid returning token
     *
     * @param request  request
     * @param response response
     * @return JSON Web Token
     */
    private String authenticate(Request request, Response response) {
        Map<String, String> result = new HashMap<>();
        String username = request.queryParamOrDefault("username", "").trim();
        String password = request.queryParamOrDefault("password", "");
        var user = userDao.getUserByNameAndPassword(username, password);
        if (user == null) {
            result.put("ok", "false");
            result.put("error", "Invalid username or/and password");
            response.status(HttpStatus.BAD_REQUEST_400);
        } else {
            result.put("ok", "true");
            result.put("token", jwtManager.generateTokenForUser(user));
            response.status(HttpStatus.OK_200);
        }
        return gson.toJson(result);
    }

    /**
     * Validating specified token and if it is ok returning User instance
     *
     * @param request request
     * @return User instance
     */
    private User authorize(Request request) {
        String token = request.queryParamOrDefault("token", "");
        return jwtManager.extractUserFromToken(token);
    }

    // :auth end


    // authors:

    private String getAuthors(Request request, Response response) {
        authorize(request);
        Map<String, Object> result = new HashMap<>();
        if (request.queryParams().contains("name")) {
            if (request.queryParams().contains("id")) {
                result.put("ok", "false");
                result.put("error", "Specify either only name or only id");
                response.status(HttpStatus.BAD_REQUEST_400);
            } else {
                result.put("ok", "true");
                result.put("authors", authorDao.getAuthorsByName(request.queryParams("name")));
                response.status(HttpStatus.OK_200);
            }
        } else if (request.queryParams().contains("id")) {
            if (Utils.isInteger(request.queryParams("id"))) {
                int id = Integer.parseInt(request.queryParams("id"));
                result.put("ok", "true");
                result.put("author", authorDao.getAuthorById(id));
                response.status(HttpStatus.OK_200);
            } else {
                result.put("ok", "false");
                result.put("error", "Invalid id");
                response.status(HttpStatus.BAD_REQUEST_400);
            }
        } else {
            result.put("ok", "true");
            result.put("authors", authorDao.getAuthors());
            response.status(HttpStatus.OK_200);
        }
        return gson.toJson(result);
    }

    // :authors end


    // books:

    private String getBooks(Request request, Response response) {
        authorize(request);
        var argsCheckError = checkGetBooksRequestParams(request);
        if (argsCheckError != null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(argsCheckError);
        }

        String isbn = request.queryParamOrDefault("isbn", null);
        String authorId = request.queryParamOrDefault("authorId", null);
        String bookName = request.queryParamOrDefault("bookName", null);
        String firstTimestamp = request.queryParamOrDefault("firstTimestamp", null);
        String secondTimestamp = request.queryParamOrDefault("secondTimestamp", null);
        boolean areTimestampsNull = firstTimestamp == null && secondTimestamp == null;

        if (isbn != null) {
            var book = bookDao.getBookByISBN(Long.parseLong(isbn));
            if (book == null) {
                return gson.toJson(Map.of("ok", "false", "error", "Book not found!"));
            }
            return gson.toJson(Map.of("ok", "true", "book", book));
        }
        Map<String, Object> result = new HashMap<>();
        result.put("ok", "true");
        response.status(HttpStatus.OK_200);
        if (authorId == null && bookName == null && areTimestampsNull) {
            result.put("ok", "false");
            result.put("error", "U haven't specified any parameter!");
        } else if (authorId != null && bookName == null && areTimestampsNull) { // only by authorId
            result.put("books", bookDao.getBooksByAuthorId(Integer.parseInt(authorId)));
        } else if (authorId == null && bookName != null && areTimestampsNull) { // only by bookName
            result.put("books", bookDao.getBooksByName(bookName));
        } else if (authorId != null && bookName != null && areTimestampsNull) { // author id and book name
            result.put("books", bookDao.getBooksByAuthorIdAndBookName(Integer.parseInt(authorId), bookName));
        } else {
            long first = firstTimestamp == null ? 0 : Long.parseLong(firstTimestamp);
            long second = secondTimestamp == null ? Integer.MAX_VALUE : Long.parseLong(secondTimestamp);
            if (authorId == null && bookName == null && !areTimestampsNull) { // only by timestamps
                result.put("books", bookDao.getBooksByDateInterval(first, second));
            } else if (authorId != null && bookName == null && !areTimestampsNull) { // author id and timestamps
                result.put("books", bookDao.getBooksByAuthorIdAndDateInterval(Integer.parseInt(authorId), first, second));
            } else if (authorId == null && bookName != null && !areTimestampsNull) { // book name and timestamps
                result.put("books", bookDao.getBooksByNameAndDateInterval(bookName, first, second));
            } else if (authorId != null && bookName != null && !areTimestampsNull) {// all!
                result.put("books", bookDao.getBooksByNameAndAuthorIdAndDateInterval(bookName, Integer.parseInt(authorId), first, second));
            }
        }
        return gson.toJson(result);
    }

    private Map checkGetBooksRequestParams(Request request) {
        if (!Utils.isLong(request.queryParamOrDefault("isbn", "0"))) {
            return Map.of("ok", "false", "error", "isbn must be long");
        }
        if (!Utils.isLong(request.queryParamOrDefault("firstTimestamp", "0"))) {
            return Map.of("ok", "false", "error", "firstTimestamp must be long");
        }
        if (!Utils.isLong(request.queryParamOrDefault("secondTimestamp", "0"))) {
            return Map.of("ok", "false", "error", "secondTimestamp must be long");
        }
        if (!Utils.isInteger(request.queryParamOrDefault("authorId", "0"))) {
            return Map.of("ok", "false", "error", "authorId must be integer");
        }
        return null;
    }

    private String deleteBook(Request request, Response response) {
        var user = authorize(request);
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN)) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return gson.toJson(Map.of("ok", "false", "error", "You have no permissions to do that"));
        }
        if (!Utils.isLong(request.params(":isbn"))) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(Map.of("ok", "false", "error", "Invalid isbn"));
        }
        long bookId = Long.parseLong(request.params(":isbn"));
        bookDao.deleteBookByIsbn(bookId);
        return gson.toJson(Map.of("ok", "true"));
    }

    private String addBook(Request request, Response response) {
        var user = authorize(request);
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN)) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return gson.toJson(Map.of("ok", "false", "error", "You have no permissions to do that"));
        }
        String isbnStr = request.queryParamOrDefault("isbn", "");
        String name = request.queryParamOrDefault("name", "");
        String author = request.queryParamOrDefault("author", "");
        Date date = Utils.toDate(request.queryParamOrDefault("date", ""));

        response.status(HttpStatus.BAD_REQUEST_400);
        if (!Utils.isLong(isbnStr)) return gson.toJson(Map.of("ok", "false", "error", "Isbn must be long"));
        if (name.isBlank()) return gson.toJson(Map.of("ok", "false", "error", "Name must be not blank"));
        if (author.isBlank()) return gson.toJson(Map.of("ok", "false", "error", "Author must be not blank"));
        if (date == null) return gson.toJson(Map.of("ok", "false", "error", "Invalid date"));

        var isbn = Long.parseLong(isbnStr);
        if (bookDao.getBookByISBN(isbn) != null) {
            response.status(HttpStatus.CONFLICT_409);
            return gson.toJson(Map.of("ok", "false", "error", "Book with that isbn already exist"));
        }

        var book = new Book(isbn, name, author, date);
        bookDao.addBookToDatabase(book);
        response.status(HttpStatus.CREATED_201);
        return gson.toJson(Map.of("ok", "true"));
    }

    // :books end


    // bookmarks:

    private String getBookmarks(Request request, Response response) {
        response.status(HttpStatus.OK_200);
        var user = authorize(request);
        var bookmarks = bookmarkDao.getUserBookmarks(user);
        if (bookmarks.isEmpty()) {
            return gson.toJson(Map.of("ok", "false", "error", "No bookmarks found"));
        }
        return gson.toJson(Map.of("ok", "true", "bookmarks", bookmarks));
    }

    private String deleteBookmark(Request request, Response response) {
        var user = authorize(request);
        if (!Utils.isInteger(request.params(":id"))) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(Map.of("ok", "false", "error", "Invalid bookmark id"));
        }
        var bookmarkId = Integer.parseInt(request.params(":id"));
        for (Bookmark userBookmark : bookmarkDao.getUserBookmarks(user)) {
            if (userBookmark.ID == bookmarkId) {
                bookmarkDao.deleteBookmarkById(bookmarkId);
                response.status(HttpStatus.OK_200);
                return gson.toJson(Map.of("ok", "true"));
            }
        }
        response.status(HttpStatus.NOT_FOUND_404);
        return gson.toJson(Map.of("ok", "false", "error", "Bookmark not found or owner of that bookmark is not you"));
    }

    private String addBookmark(Request request, Response response) {
        var user = authorize(request);

        var bookIsbnStr = request.queryParamOrDefault("bookIsbn", "");
        var pageStr = request.queryParamOrDefault("page", "");

        response.status(HttpStatus.BAD_REQUEST_400);
        if (!Utils.isLong(bookIsbnStr)) return gson.toJson(Map.of("ok", "false", "error", "Invalid isbn"));
        if (!Utils.isInteger(pageStr)) return gson.toJson(Map.of("ok", "false", "error", "Invalid page"));
        var book = bookDao.getBookByISBN(Long.parseLong(bookIsbnStr));
        if (book == null) return gson.toJson(Map.of("ok", "false", "error", "Book with specified isbn not found"));

        bookmarkDao.addBookmark(user, book, Integer.parseInt(pageStr));
        response.status(HttpStatus.CREATED_201);
        return gson.toJson(Map.of("ok", "true"));
    }

    // :bookmarks end


    // users:

    private String getUsers(Request request, Response response) {
        var user = authorize(request);
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN)) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return gson.toJson(Map.of("ok", "false", "error", "You have no permissions to do that"));
        }
        var usernamePattern = request.params(":name");
        if (usernamePattern == null || usernamePattern.isEmpty()) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(Map.of("ok", "false", "error", "Invalid username pattern provided!"));
        }
        response.status(HttpStatus.OK_200);
        if (".".equals(usernamePattern)) return gson.toJson(Map.of("ok", "true", "users", userDao.getUsersByNamePattern("")));
        return gson.toJson(Map.of("ok", "true", "users", userDao.getUsersByNamePattern(usernamePattern)));
    }

    private String addUser(Request request, Response response) {
        var user = authorize(request);
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN)) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return gson.toJson(Map.of("ok", "false", "error", "You have no permissions to do that"));
        }
        var username = request.queryParamOrDefault("username", "");
        var password = request.queryParamOrDefault("password", "");
        if (!username.matches("^[0-9A-zА-я]{3,20}$")) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(Map.of("ok", "false", "error", "Invalid username"));
        }
        if (password.equals("")) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return gson.toJson(Map.of("ok", "false", "error", "Password must be not empty"));
        }
        if (userDao.getUserByName(username) != null) {
            response.status(HttpStatus.CONFLICT_409);
            return gson.toJson(Map.of("ok", "false", "error", "User with specified name already exist"));
        }
        User newUser = new User(228, username, User.Role.BASE, DigestUtils.md5Hex(password));
        userDao.addUserToDatabase(newUser);
        response.status(HttpStatus.CREATED_201);
        return gson.toJson(Map.of("ok", "true"));

    }

    private String changeUserRole(Request request, Response response) {
        var user = authorize(request);
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN)) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return gson.toJson(Map.of("ok", "false", "error", "You have no permissions to do that"));
        }
        var role = Utils.toRole(request.queryParamOrDefault("role", ""));
        var username = request.params(":name");

        response.status(HttpStatus.BAD_REQUEST_400);
        if (role == null) return gson.toJson(Map.of("ok", "false", "error", "Unknown role"));
        if (username.isBlank()) return gson.toJson(Map.of("ok", "false", "error", "Username must be not blank"));
        if (!user.ROLE.hasParentOrEquals(role))
            return gson.toJson(Map.of("ok", "false", "error", "You can give to user role that equals your role or \"smaller\" than it"));
        var targetUser = userDao.getUserByName(username);
        if (targetUser == null) return gson.toJson(Map.of("ok", "false", "error", "Target user not found"));

        userDao.changeUserRole(targetUser, role);
        response.status(HttpStatus.OK_200);
        return gson.toJson(Map.of("ok", "true"));
    }

    // :users end
}
