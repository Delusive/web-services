package com.epam.webservices.rest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.webservices.entity.User;
import com.epam.webservices.rest.exception.InvalidTokenException;


import java.util.Date;


public class JwtManager {
    private final int EXPIRES_MINS = 60;
    private final Algorithm algorithm;

    public JwtManager(String secret) {
        algorithm = Algorithm.HMAC256(secret);
    }

    public String generateTokenForUser(User user) {
        return JWT.create()
                .withClaim("id", user.ID)
                .withClaim("name", user.NAME)
                .withClaim("role", user.ROLE.toString())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRES_MINS * 60 * 1000))
                .sign(algorithm);
    }

    public User extractUserFromToken(String token) throws InvalidTokenException {
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            int id = jwt.getClaim("id").asInt();
            String name = jwt.getClaim("name").asString();
            String role = jwt.getClaim("role").asString();
            return new User(id, name, User.Role.valueOf(role), role);
        } catch (JWTVerificationException | IllegalArgumentException e) {
            throw new InvalidTokenException(e);
        }
    }


}
