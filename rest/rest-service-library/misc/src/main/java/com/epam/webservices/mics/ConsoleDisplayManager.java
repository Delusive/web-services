package com.epam.webservices.mics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleDisplayManager implements IDisplayManager {
    private final Logger LOGGER = LogManager.getLogger();

    @Override
    public void displayMessage(String str) {
        LOGGER.info(str);
    }

    @Override
    public void displayFormattedMessage(String format, Object... args) {
        String message = String.format(format, args);
        LOGGER.info(message);
    }

    @Override
    public void displayError(Throwable throwable) {
        LOGGER.error(throwable);
    }

}
