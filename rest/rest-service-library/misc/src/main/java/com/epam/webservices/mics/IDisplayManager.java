package com.epam.webservices.mics;

public interface IDisplayManager {
    void displayMessage(String message);
    void displayFormattedMessage(String format, Object... args);
    void displayError(Throwable e);
}
