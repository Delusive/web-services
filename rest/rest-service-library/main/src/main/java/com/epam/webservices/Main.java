package com.epam.webservices;

import com.epam.webservices.connectivity.IDatabaseManager;
import com.epam.webservices.connectivity.MySQLDatabaseManager;
import com.epam.webservices.dao.IAuthorDao;
import com.epam.webservices.dao.IBookDao;
import com.epam.webservices.dao.IBookmarkDao;
import com.epam.webservices.dao.IUserDao;
import com.epam.webservices.dao.impl.AuthorDao;
import com.epam.webservices.dao.impl.BookDao;
import com.epam.webservices.dao.impl.BookmarkDao;
import com.epam.webservices.dao.impl.UserDao;
import com.epam.webservices.mics.ConsoleDisplayManager;
import com.epam.webservices.mics.IDisplayManager;
import com.epam.webservices.mics.PropertiesManager;
import com.epam.webservices.rest.JwtManager;
import com.epam.webservices.rest.RestService;

public class Main {

    public static void main(String[] args) {
        final PropertiesManager config = new PropertiesManager("/library.properties");
        final JwtManager jwtManager = new JwtManager(config.getString("auth.jwt.secret"));
        final IDatabaseManager databaseManager = new MySQLDatabaseManager(config.getProperties());
        final IAuthorDao authorDao = new AuthorDao(databaseManager);
        final IBookDao bookDao = new BookDao(databaseManager);
        final IBookmarkDao bookmarkDao = new BookmarkDao(databaseManager);
        final IUserDao userDao = new UserDao(databaseManager);

        final RestService restService = new RestService(config.getString("rest.ip"), config.getInt("rest.port"), jwtManager, authorDao, bookDao, bookmarkDao, userDao);
        restService.init();

    }
}
