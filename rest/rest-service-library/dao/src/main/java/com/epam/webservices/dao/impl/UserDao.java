package com.epam.webservices.dao.impl;

import com.epam.webservices.connectivity.IDatabaseManager;
import com.epam.webservices.dao.IUserDao;
import com.epam.webservices.entity.User;
import com.epam.webservices.mics.IDisplayManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements IUserDao {
    private final IDatabaseManager DBMANAGER;

    public UserDao(IDatabaseManager dbManager) {
        this.DBMANAGER = dbManager;
    }

    @Override
    public User getUserByNameAndPassword(String username, String password) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `userId`, `username`, `password`, `rolename` FROM `users` JOIN `roles` USING (`roleId`) WHERE `username` = ? AND `password` = MD5(?) LIMIT 1;";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, username);
                stmt.setString(2, password);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    return extractUserFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User getUserById(int id) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `userId`, `username`, `password`, `rolename` FROM `users` JOIN `roles` USING (`roleId`) WHERE `userId` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, id);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    return extractUserFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean addUserToDatabase(User user) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "INSERT INTO `users` (`username`, `password`, `roleId`) VALUES (?, ?, (SELECT `roleId` FROM `roles` WHERE `rolename` = ?))";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, user.NAME);
                stmt.setString(2, user.PASSWORD_HASH);
                stmt.setString(3, user.ROLE.toString());
                stmt.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> getUsersByNamePattern(String namePattern) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `userId`, `username`, `password`, `rolename` FROM `users` JOIN `roles` USING (`roleId`) WHERE `username` LIKE ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, "%" + namePattern + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<User> result = new ArrayList<>();
                    User user;
                    while ((user = extractUserFromResultSet(resultSet)) != null) {
                        result.add(user);
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean changeUserRole(User user, User.Role toRole) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "UPDATE `users` SET `roleId` = (SELECT `roleId` FROM `roles` WHERE `rolename` = ?) WHERE userId = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, toRole.toString());
                stmt.setInt(2, user.ID);
                stmt.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User getUserByName(String username) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `userId`, `username`, `password`, `rolename` FROM `users` JOIN `roles` USING (`roleId`) WHERE `username` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, username);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    return extractUserFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private User extractUserFromResultSet(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) return null;
        int userId = resultSet.getInt("userId");
        String userName = resultSet.getString("username");
        User.Role role = User.Role.valueOf(resultSet.getString("rolename"));
        String passwordHash = resultSet.getString("password");
        return new User(userId, userName, role, passwordHash);
    }

}
