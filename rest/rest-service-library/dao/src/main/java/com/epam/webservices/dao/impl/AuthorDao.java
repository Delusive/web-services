package com.epam.webservices.dao.impl;

import com.epam.webservices.connectivity.IDatabaseManager;
import com.epam.webservices.dao.IAuthorDao;
import com.epam.webservices.entity.Author;
import com.epam.webservices.mics.IDisplayManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDao implements IAuthorDao {
    private final IDatabaseManager DBMANAGER;

    public AuthorDao(IDatabaseManager dbManager) {
        this.DBMANAGER = dbManager;
    }

    @Override
    public List<Author> getAuthors() {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors`";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Author> result = new ArrayList<>();
                    while (resultSet.next()) {
                        int authorId = resultSet.getInt("authorId");
                        String authorName = resultSet.getString("authorName");
                        result.add(new Author(authorId, authorName));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Author getAuthorById(int id) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors` WHERE `authorId` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, id);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    if (!resultSet.next()) return null;
                    int authorId = resultSet.getInt("authorId");
                    String authorName = resultSet.getString("authorName");
                    return new Author(authorId, authorName);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Author> getAuthorsByName(String namePattern) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors` WHERE `authorName` LIKE ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, "%" + namePattern + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Author> result = new ArrayList<>();
                    while (resultSet.next()) {
                        int authorId = resultSet.getInt("authorId");
                        String authorName = resultSet.getString("authorName");
                        result.add(new Author(authorId, authorName));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
