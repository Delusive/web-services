package com.epam.jdbc.entity;

public class Bookmark extends Entity {
    public final User USER;
    public final Book BOOK;
    public final int PAGE;

    public Bookmark(User user, Book book, int page) {
        this.USER = user;
        this.BOOK = book;
        this.PAGE = page;
    }

    @Override
    public String toString() {
        return BOOK.toString() + " | Page: " + PAGE;
    }
}
