package com.epam.jdbc.ui;

import com.epam.jdbc.command.*;
import com.epam.jdbc.dto.IAuthorDto;
import com.epam.jdbc.dto.IBookDto;
import com.epam.jdbc.dto.IBookmarkDto;
import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.dto.impl.AuthorDto;
import com.epam.jdbc.dto.impl.BookDto;
import com.epam.jdbc.dto.impl.BookmarkDto;
import com.epam.jdbc.dto.impl.UserDto;
import com.epam.webservices.soap.*;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.ConsoleDisplayManager;
import com.epam.jdbc.mics.IDisplayManager;
import com.epam.jdbc.mics.PropertiesManager;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class UserInterface implements IUserInterface {
    private IDisplayManager displayManager = new ConsoleDisplayManager();
    private PropertiesManager config = new PropertiesManager("/library.properties", displayManager);
    private PropertiesManager messages = new PropertiesManager("/messages.properties", displayManager);
    SoapService soapPort = new SoapServiceImplService().getSoapServiceImplPort();
    private IUserDto userDto = new UserDto(soapPort);
    private IAuthorDto authorDto = new AuthorDto(soapPort);
    private IBookDto bookDto = new BookDto(soapPort, authorDto);
    private IBookmarkDto bookmarkDto = new BookmarkDto(soapPort, bookDto, userDto);
    private User user;
    private String password; //За такое можно и убить

    @Override
    public void delegate() {
        Scanner scanner = new Scanner(System.in);
        authentication(scanner);
        registerCommands();
        while (true) {
            displayManager.displayMessage(messages.getString("menu.header.top"));
            displayManager.displayMessage(messages.getString("menu.header.middle"));
            displayManager.displayMessage(messages.getString("menu.header.bot"));
            showMenu();
            displayManager.displayMessage(messages.getString("menu.footer.bot"));
            while (true) {
                int choseActionNumber;
                try {
                    choseActionNumber = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    displayManager.displayMessage(messages.getString("menu.notanumber"));
                    continue;
                }
                //if he want to logout:
                if (choseActionNumber == 0) {
                    user = null;
                    authentication(scanner);
                    break;
                }
                processCommand(choseActionNumber);
                displayManager.displayMessage(messages.getString("menu.pressenter"));
                scanner.nextLine();
                authorize(user.NAME, password, scanner);
                break;
            }
        }
    }

    private void registerCommands() {
        Properties messages = this.messages.getProperties();
        CommandManager.unregisterCommands();
        CommandManager.registerCommand(new SearchBooksAndMakingBookmarks(bookDto, authorDto, bookmarkDto, displayManager, user, messages));
        CommandManager.registerCommand(new AddBookToDB(bookDto, displayManager, messages));
        CommandManager.registerCommand(new ShowAndDeleteBookmarks(displayManager, bookmarkDto, user, messages));
        CommandManager.registerCommand(new DeleteBookFromDB(displayManager, bookDto, messages));
        CommandManager.registerCommand(new AddNewUser(displayManager, userDto, messages));
        CommandManager.registerCommand(new ChangeUserRole(displayManager, userDto, user, messages));
    }

    private void processCommand(int actionNumber) {
        List<IExecutableCommand> commands = CommandManager.getAllowedCommandsFor(user.ROLE);
        if (actionNumber > commands.size() || actionNumber < 1) {
            displayManager.displayMessage(messages.getString("menu.invalidchoice"));
            return;
        }
        commands.get(actionNumber - 1).execute();
    }

    private void showMenu() {
        StringBuilder sb = new StringBuilder("* 0. Logout");
        while (sb.length() != messages.getString("menu.header.bot").length() - 1) sb.append(' ');
        sb.append('*');
        displayManager.displayMessage(sb.toString());

        int i = 1;
        for (IExecutableCommand c : CommandManager.getAllowedCommandsFor(user.ROLE)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("* ").append(i).append(". ").append(c.getDescription());
            while (stringBuilder.length() != messages.getString("menu.header.bot").length() - 1)
                stringBuilder.append(' ');
            stringBuilder.append('*');
            displayManager.displayMessage(stringBuilder.toString());
            i++;
        }
    }

    private void authentication(Scanner scanner) {
        displayManager.displayMessage(messages.getString("menu.authrequest.username"));
        String username = scanner.nextLine();
        displayManager.displayMessage(messages.getString("menu.authrequest.password"));
        while (true) {
            String password = scanner.nextLine();
            if (password.equalsIgnoreCase(":rename")) {
                authentication(scanner);
                return;
            }
            User user = userDto.authenticate(username, password);
            if (user != null) {
                this.user = user;
                this.password = password;
                displayManager.displayFormattedMessage(messages.getString("menu.authrequest.success")
                        .replaceAll("%username%", user.NAME)
                        .replaceAll("%role%", user.ROLE.toString()));
                registerCommands();
                break;
            }
            displayManager.displayMessage(messages.getString("menu.authrequest.error"));
        }

    }

    private void authorize(String username, String password, Scanner scanner) {
        user = userDto.authenticate(username, password);
        if (user == null) {
            displayManager.displayMessage(messages.getString("authentication.required"));
            authentication(scanner);
            return;
        }
        registerCommands();
    }
}
