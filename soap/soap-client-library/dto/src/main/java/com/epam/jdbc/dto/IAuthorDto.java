package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Author;

import java.util.List;

public interface IAuthorDto extends IBaseDto {
    List<Author> getAuthors();

    Author getAuthorById(int id);

    List<Author> getAuthorsByName(String namePattern);
}
