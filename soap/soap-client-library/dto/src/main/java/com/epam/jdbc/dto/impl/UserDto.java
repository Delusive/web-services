package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.entity.User;
import com.epam.webservices.soap.RoleEnum;
import com.epam.webservices.soap.SoapService;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.ws.BindingProvider;
import java.util.ArrayList;
import java.util.List;

public class UserDto implements IUserDto {
    private final SoapService port;

    public UserDto(SoapService port) {
        this.port = port;
    }

    @Override
    public User getUserById(int id) {
        try {
            return toNormalUser(port.getUserById(id));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean addUserToDatabase(User user) {
        try {
            port.createUser(toParanormalUser(user));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<User> getUsersByNamePattern(String namePattern) {
        List<User> users = new ArrayList<>();
        try {
            port.getAllUser().forEach(user -> {
                if (user.getLOGIN().matches(".*" + namePattern + ".*")) users.add(toNormalUser(user));
            });
            return users;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean changeUserRole(User user, User.Role toRole) {
        try {
            if (toRole == User.Role.ADMIN) return port.toMakeTheAdmin(user.NAME);
            if (toRole == User.Role.BASE) return port.toMakeTheUser(user.NAME);
            throw new IllegalArgumentException("Invalid role provided");
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public User authenticate(String username, String password) {
        BindingProvider prov = (BindingProvider) port;
        prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
        prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);
        try {
            return toNormalUser(port.getAuthenticatedUser());
        } catch (Exception e) {
            return null;
        }
    }

    private User toNormalUser(com.epam.webservices.soap.User user) {
        int id = user.getUSERID();
        String name = user.getLOGIN();
        String pass = user.getPASSWORD();
        User.Role role = user.getROLE() == RoleEnum.ADMIN ? User.Role.ADMIN : User.Role.BASE;
        return new User(id, name, role, DigestUtils.md5Hex(pass));
    }

    private com.epam.webservices.soap.User toParanormalUser(User user) {
        com.epam.webservices.soap.User paranormalUser = new com.epam.webservices.soap.User();
        paranormalUser.setUSERID(user.ID);
        paranormalUser.setLOGIN(user.NAME);
        paranormalUser.setPASSWORD(null); //У меня есть только хэш, а тут, полагаю, нужен пароль в чистом виде
        paranormalUser.setROLE(user.ROLE == User.Role.ADMIN ? RoleEnum.ADMIN : RoleEnum.USER);
        return paranormalUser;
    }
}
