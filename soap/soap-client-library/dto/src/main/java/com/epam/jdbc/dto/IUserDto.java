package com.epam.jdbc.dto;

import com.epam.jdbc.entity.User;

import java.util.List;

public interface IUserDto extends IBaseDto {
    User getUserById(int id);

    boolean addUserToDatabase(User user);

    List<User> getUsersByNamePattern(String namePattern);

    boolean changeUserRole(User user, User.Role toRole);

    User authenticate(String username, String password);
}
