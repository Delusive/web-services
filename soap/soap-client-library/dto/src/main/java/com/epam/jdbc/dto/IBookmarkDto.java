package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;

import java.util.List;

public interface IBookmarkDto extends IBaseDto {
    List<Bookmark> getUserBookmarks(User user);

    boolean deleteBookmarkByBookIsbn(int bookIsbn);

    boolean addBookmark(User user, Book book, int page);
}
