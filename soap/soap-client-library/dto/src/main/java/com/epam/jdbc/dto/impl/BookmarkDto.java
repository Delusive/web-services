package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IBookDto;
import com.epam.jdbc.dto.IBookmarkDto;
import com.epam.jdbc.dto.IUserDto;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;
import com.epam.webservices.soap.SoapService;

import java.util.ArrayList;
import java.util.List;

public class BookmarkDto implements IBookmarkDto {
    private final SoapService port;
    private final IBookDto bookDto;
    private final IUserDto userDto;

    public BookmarkDto(SoapService port, IBookDto bookDto, IUserDto userDto) {
        this.port = port;
        this.bookDto = bookDto;
        this.userDto = userDto;
    }

    @Override
    public List<Bookmark> getUserBookmarks(User user) {
        try {
            List<Bookmark> bookmarks = new ArrayList<>();
            port.getBookmarkByLogin(user.NAME).forEach(bookmark -> {
                if (bookmark.isActive()) bookmarks.add(toNormalBookmark(bookmark));
            });
            return bookmarks;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean deleteBookmarkByBookIsbn(int bookIsbn) {
        try {
            return port.deleteBookmark(bookIsbn);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean addBookmark(User user, Book book, int page) {
        try {
            return port.createBookmark((int) book.ISBN, page);
        } catch (Exception e) {
            return false;
        }
    }

    private Bookmark toNormalBookmark(com.epam.webservices.soap.Bookmark bookmark) {
        User user = userDto.getUserById(bookmark.getUSERID());
        Book book = bookDto.getBookByISBN(bookmark.getISBN());
        int page = bookmark.getPAGE();
        return new Bookmark(user, book, page);
    }

    private com.epam.webservices.soap.Bookmark toParanormalBookmark(Bookmark bookmark) {
        com.epam.webservices.soap.Bookmark paranormalBookmark = new com.epam.webservices.soap.Bookmark();
        paranormalBookmark.setISBN((int) bookmark.BOOK.ISBN);
        paranormalBookmark.setUSERID(bookmark.USER.ID);
        paranormalBookmark.setPAGE(bookmark.PAGE);
        return paranormalBookmark;
    }
}
