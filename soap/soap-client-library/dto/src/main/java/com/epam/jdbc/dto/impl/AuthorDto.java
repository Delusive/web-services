package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IAuthorDto;
import com.epam.jdbc.entity.Author;
import com.epam.webservices.soap.SoapService;

import java.util.ArrayList;
import java.util.List;

public class AuthorDto implements IAuthorDto {
    private final SoapService port;

    public AuthorDto(SoapService port) {
        this.port = port;
    }

    @Override
    public List<Author> getAuthors() {
        List<Author> authors = new ArrayList<>();
        port.getAllAuthors().forEach(author -> authors.add(toNormalAuthor(author)));
        return authors;
    }

    @Override
    public Author getAuthorById(int id) {
        for (Author author : getAuthors()) {
            if (author.ID == id) return author;
        }
        return null;
    }

    @Override
    public List<Author> getAuthorsByName(String namePattern) {
        List<Author> authors = new ArrayList<>();
        for (Author author : getAuthors()) {
            if (author.NAME.matches(".*" + namePattern + ".*")) authors.add(author);
        }
        return authors;
    }

    private Author toNormalAuthor(com.epam.webservices.soap.Author author) {
        int id = author.getAUTHORID();
        String name = author.getNAME();
        return new Author(id, name);
    }
}
