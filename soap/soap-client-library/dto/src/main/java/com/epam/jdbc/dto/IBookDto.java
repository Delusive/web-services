package com.epam.jdbc.dto;

import com.epam.jdbc.entity.Book;

import java.util.List;

public interface IBookDto extends IBaseDto {

    Book getBookByISBN(int isbn);

    List<Book> getBooksByAuthorId(int authorId);

    List<Book> getBooksByName(String bookNamePattern);

    List<Book> getBooksByDateInterval(long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByAuthorIdAndBookName(int authorId, String bookName);

    List<Book> getBooksByAuthorIdAndDateInterval(int authorId, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndDateInterval(String bookNamePattern, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndAuthorIdAndDateInterval(String bookName, int authorId, long firstTimestamp, long secondTimestamp);

    boolean deleteBookByIsbn(int isbn);

    boolean addBookToDatabase(Book book);

}
