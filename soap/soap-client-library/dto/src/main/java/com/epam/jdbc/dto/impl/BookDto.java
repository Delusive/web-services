package com.epam.jdbc.dto.impl;

import com.epam.jdbc.dto.IAuthorDto;
import com.epam.jdbc.dto.IBookDto;
import com.epam.jdbc.entity.Book;
import com.epam.webservices.soap.SoapService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class BookDto implements IBookDto {
    private final SoapService port;
    private final IAuthorDto authorDto;

    public BookDto(SoapService port, IAuthorDto authorDto) {
        this.port = port;
        this.authorDto = authorDto;
    }


    @Override
    public Book getBookByISBN(int isbn) {
        try {
            List<com.epam.webservices.soap.Book> books = port.findBookWithConditions(isbn, null, null, 0, 0);
            if (books.isEmpty()) return null;
            return toNormalBook(books.get(0));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByAuthorId(int authorId) {
        try {
            String authorName = authorDto.getAuthorById(authorId).NAME;
            List<Book> books = new ArrayList<>();
            port.findBookWithConditions(0, null, authorName, 0, 0)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByName(String bookNamePattern) {
        try {
            List<Book> books = new ArrayList<>();
            port.findBookWithConditions(0, bookNamePattern, null, 0, 0)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByDateInterval(long firstTimestamp, long secondTimestamp) {
        try {
            List<Book> books = new ArrayList<>();
            int firstYear = (int) (1970 + (firstTimestamp / 1000 / 86400 / 365));
            int secondYear = (int) (1970 + (secondTimestamp / 1000 / 86400 / 365));
            port.findBookWithConditions(0, null, null, firstYear, secondYear)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByAuthorIdAndBookName(int authorId, String bookName) {
        try {
            String authorName = authorDto.getAuthorById(authorId).NAME;
            List<Book> books = new ArrayList<>();
            port.findBookWithConditions(0, bookName, authorName, 0, 0)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByAuthorIdAndDateInterval(int authorId, long firstTimestamp, long secondTimestamp) {
        try {
            List<Book> books = new ArrayList<>();
            String authorName = authorDto.getAuthorById(authorId).NAME;
            int firstYear = (int) (1970 + (firstTimestamp / 1000 / 86400 / 365));
            int secondYear = (int) (1970 + (secondTimestamp / 1000 / 86400 / 365));
            port.findBookWithConditions(0, null, authorName, firstYear, secondYear)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByNameAndDateInterval(String bookNamePattern, long firstTimestamp, long secondTimestamp) {
        try {
            List<Book> books = new ArrayList<>();
            int firstYear = (int) (1970 + (firstTimestamp / 1000 / 86400 / 365));
            int secondYear = (int) (1970 + (secondTimestamp / 1000 / 86400 / 365));
            port.findBookWithConditions(0, bookNamePattern, null, firstYear, secondYear)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByNameAndAuthorIdAndDateInterval(String bookNamePattern, int authorId, long firstTimestamp, long secondTimestamp) {
        try {
            List<Book> books = new ArrayList<>();
            String authorName = authorDto.getAuthorById(authorId).NAME;
            int firstYear = (int) (1970 + (firstTimestamp / 1000 / 86400 / 365));
            int secondYear = (int) (1970 + (secondTimestamp / 1000 / 86400 / 365));
            port.findBookWithConditions(0, bookNamePattern, authorName, firstYear, secondYear)
                    .forEach(book -> books.add(toNormalBook(book)));
            return books;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean deleteBookByIsbn(int isbn) {
        try {
            return port.deleteBook(isbn);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean addBookToDatabase(Book book) {
        try {
            return port.addBook(toParanormalBook(book));
        } catch (Exception e) {
            return false;
        }
    }

    private Book toNormalBook(com.epam.webservices.soap.Book book) {
        int isbn = book.getISBN();
        String name = book.getTITLEBOOK();
        String authorName = book.getAUTHOR();
        Date issueDate = Date.valueOf(book.getPUBLDATE());
        int pageCount = book.getPAGECOUNT();
        return new Book(isbn, name, authorName, issueDate, pageCount);
    }

    private com.epam.webservices.soap.Book toParanormalBook(Book book) {
        com.epam.webservices.soap.Book paranormalBook = new com.epam.webservices.soap.Book();
        paranormalBook.setISBN((int) book.ISBN); // TODO: 10/16/2019 Проверить, что происходит, когда вводится большой isbn
        paranormalBook.setTITLEBOOK(book.NAME);
        paranormalBook.setAUTHOR(book.AUTHOR);
        paranormalBook.setPUBLDATE(book.DATE.toLocalDate().toString()); // TODO: 10/16/2019 Почему тут требуется String, если Вика использует в качестве даты только год, т.е. int?
        paranormalBook.setPAGECOUNT(book.PAGE_COUNT);
        return paranormalBook;
    }
}
