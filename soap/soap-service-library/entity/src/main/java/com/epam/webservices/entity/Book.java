package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

public class Book extends Entity {
    @SerializedName("isbn")
    public long ISBN;
    @SerializedName("name")
    public String NAME;
    @SerializedName("author")
    public String AUTHOR;
    @SerializedName("date")
    public Date DATE;

    public Book(long isbn, String name, String author, Date date) {
        this.ISBN = isbn;
        this.NAME = name;
        this.AUTHOR = author;
        this.DATE = date;
    }

    public Book() {
        ISBN = 0;
        NAME = null;
        AUTHOR = null;
        DATE = null;
    }

    @Override
    public String toString() {
        return String.format("ISBN: %s | Name: %s | Author: %s | Publish Date: %s",
                ISBN,
                NAME,
                AUTHOR,
                DATE);
    }
}
