package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Author extends Entity {
    @SerializedName("id")
    public final int ID;
    @SerializedName("name")
    public final String NAME;

    public Author(int id, String name) {
        this.ID = id;
        this.NAME = name;
    }
}
