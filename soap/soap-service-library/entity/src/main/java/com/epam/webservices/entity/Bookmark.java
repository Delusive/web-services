package com.epam.webservices.entity;

import com.google.gson.annotations.SerializedName;

public class Bookmark extends Entity {
    @SerializedName("id")
    public final int ID;
    @SerializedName("user")
    public final transient User USER;
    @SerializedName("book")
    public final Book BOOK;
    @SerializedName("page")
    public final int PAGE;

    public Bookmark(int id, User user, Book book, int page) {
        this.ID = id;
        this.USER = user;
        this.BOOK = book;
        this.PAGE = page;
    }

    @Override
    public String toString() {
        return BOOK.toString() + " | Page: " + PAGE;
    }
}
