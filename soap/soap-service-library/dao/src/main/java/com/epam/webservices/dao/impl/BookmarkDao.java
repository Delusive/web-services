package com.epam.webservices.dao.impl;

import com.epam.webservices.connectivity.IDatabaseManager;
import com.epam.webservices.dao.IBookmarkDao;
import com.epam.webservices.entity.Book;
import com.epam.webservices.entity.Bookmark;
import com.epam.webservices.entity.User;
import com.epam.webservices.mics.IDisplayManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookmarkDao implements IBookmarkDao {
    private final IDatabaseManager DBMANAGER;

    public BookmarkDao(IDatabaseManager dbmanager) {
        DBMANAGER = dbmanager;
    }

    @Override
    public List<Bookmark> getUserBookmarks(User user) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date`, `bookmarkId`, `page` FROM `bookmarks` JOIN `books` ON `bookId` = `isbn` JOIN `authors` USING (`authorId`) WHERE `userId` = ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, user.ID);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Bookmark> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookmarkFromResultSetAndUser(resultSet, user));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean deleteBookmarkById(int id) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "DELETE FROM `bookmarks` WHERE `bookmarkId` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, id);
                stmt.execute();
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean addBookmark(User user, Book book, int page) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "INSERT INTO `bookmarks` (`userId`, `bookId`, `page`) VALUES (?, ?, ?)";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, user.ID);
                stmt.setLong(2, book.ISBN);
                stmt.setInt(3, page);
                stmt.execute();
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Bookmark extractBookmarkFromResultSetAndUser(ResultSet resultSet, User user) throws SQLException {
        long isbn = resultSet.getLong("isbn");
        String bookName = resultSet.getString("bookName");
        String authorName = resultSet.getString("authorName");
        Date date = resultSet.getDate("date");
        Book book = new Book(isbn, bookName, authorName, date);

        int id = resultSet.getInt("bookmarkId");
        int page = resultSet.getInt("page");

        return new Bookmark(id, user, book, page);
    }
}
