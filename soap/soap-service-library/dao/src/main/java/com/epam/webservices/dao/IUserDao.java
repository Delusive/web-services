package com.epam.webservices.dao;

import com.epam.webservices.entity.User;

import java.util.List;

public interface IUserDao extends IBaseDao {
    User getUserByNameAndPassword(String username, String password);

    User getUserById(int id);

    boolean addUserToDatabase(User user);

    List<User> getUsersByNamePattern(String namePattern);

    boolean changeUserRole(User user, User.Role toRole);

    User getUserByName(String username);
}
