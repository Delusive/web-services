package com.epam.webservices.dao;

import com.epam.webservices.entity.Book;
import com.epam.webservices.entity.Bookmark;
import com.epam.webservices.entity.User;

import java.util.List;

public interface IBookmarkDao extends IBaseDao {
    List<Bookmark> getUserBookmarks(User user);

    boolean deleteBookmarkById(int id);

    boolean addBookmark(User user, Book book, int page);
}
