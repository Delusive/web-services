package com.epam.webservices.dao;

import com.epam.webservices.entity.Author;

import java.util.List;

public interface IAuthorDao extends IBaseDao {
    List<Author> getAuthors();

    Author getAuthorById(int id);

    List<Author> getAuthorsByName(String namePattern);
}
