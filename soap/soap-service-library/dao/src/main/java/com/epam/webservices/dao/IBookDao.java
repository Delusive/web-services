package com.epam.webservices.dao;

import com.epam.webservices.entity.Book;

import java.util.List;

public interface IBookDao extends IBaseDao {

    Book getBookByISBN(long isbn);

    List<Book> getBooksByAuthorId(int authorId);

    List<Book> getBooksByName(String bookNamePattern);

    List<Book> getBooksByDateInterval(long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByAuthorIdAndBookName(int authorId, String bookName);

    List<Book> getBooksByAuthorIdAndDateInterval(int authorId, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndDateInterval(String bookNamePattern, long firstTimestamp, long secondTimestamp);

    List<Book> getBooksByNameAndAuthorIdAndDateInterval(String bookName, int authorId, long firstTimestamp, long secondTimestamp);

    boolean deleteBookByIsbn(long isbn);

    boolean addBookToDatabase(Book book);

}
