package com.epam.webservices;

import com.epam.webservices.connectivity.IDatabaseManager;
import com.epam.webservices.connectivity.MySQLDatabaseManager;
import com.epam.webservices.dao.IAuthorDao;
import com.epam.webservices.dao.IBookDao;
import com.epam.webservices.dao.IBookmarkDao;
import com.epam.webservices.dao.IUserDao;
import com.epam.webservices.dao.impl.AuthorDao;
import com.epam.webservices.dao.impl.BookDao;
import com.epam.webservices.dao.impl.BookmarkDao;
import com.epam.webservices.dao.impl.UserDao;
import com.epam.webservices.mics.PropertiesManager;
import com.epam.webservices.soap.SoapService;

import javax.xml.ws.Endpoint;

public class Main {

    public static void main(String[] args) {
        PropertiesManager config = new PropertiesManager("/library.properties");
        IDatabaseManager databaseManager = new MySQLDatabaseManager(config.getProperties());
        IUserDao userDao = new UserDao(databaseManager);
        IBookmarkDao bookmarkDao = new BookmarkDao(databaseManager);
        IBookDao bookDao = new BookDao(databaseManager);
        IAuthorDao authorDao = new AuthorDao(databaseManager);

        Endpoint.publish(config.getString("soap.url"), new SoapService(userDao, bookmarkDao, bookDao, authorDao));
    }
}
