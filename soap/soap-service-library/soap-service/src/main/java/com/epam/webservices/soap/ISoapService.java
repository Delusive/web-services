package com.epam.webservices.soap;

import com.epam.webservices.entity.Author;
import com.epam.webservices.entity.Book;
import com.epam.webservices.entity.Bookmark;
import com.epam.webservices.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding
public interface ISoapService {
    @WebMethod
    List<Author> getAuthors();

    @WebMethod
    List<Bookmark> getBookmarks();

    @WebMethod
    List<Book> getBooks(@WebParam(name = "isbn") long ISBN, @WebParam(name = "name") String namePattern, @WebParam(name = "authorId") int authorId, @WebParam(name = "firstTimestamp") long firstTimestamp, @WebParam(name = "secondTimestamp") long secondTimestamp);

    @WebMethod
    List<User> getUsers(@WebParam(name = "name") String namePattern);

    @WebMethod
    boolean addUser(@WebParam(name = "user") User targetUser);

    @WebMethod
    boolean addBook(@WebParam(name = "book") Book book);

    @WebMethod
    boolean addBookmark(@WebParam(name = "book") Book book, @WebParam(name = "page") int page);

    @WebMethod
    boolean deleteBookmark(@WebParam(name = "id") int id);

    @WebMethod
    boolean deleteBook(@WebParam(name = "isbn") long isbn);

    @WebMethod
    boolean changeUserRole(@WebParam(name = "user") User targetUser, @WebParam(name = "role") User.Role targetRole);

    @WebMethod
    User getAuthenticatedUser();
}
