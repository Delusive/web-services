package com.epam.webservices.soap;

import com.epam.webservices.dao.IAuthorDao;
import com.epam.webservices.dao.IBookDao;
import com.epam.webservices.dao.IBookmarkDao;
import com.epam.webservices.dao.IUserDao;
import com.epam.webservices.entity.Author;
import com.epam.webservices.entity.Book;
import com.epam.webservices.entity.Bookmark;
import com.epam.webservices.entity.User;
import com.epam.webservices.soap.exception.AuthorizationException;
import com.epam.webservices.soap.exception.OperationException;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@WebService(endpointInterface = "com.epam.webservices.soap.ISoapService")
public class SoapService implements ISoapService {
    private final Logger logger = LogManager.getLogger();
    private final IUserDao userDao;
    private final IBookmarkDao bookmarkDao;
    private final IBookDao bookDao;
    private final IAuthorDao authorDao;

    @Resource
    private WebServiceContext wsctx;

    public SoapService(IUserDao userDao, IBookmarkDao bookmarkDao, IBookDao bookDao, IAuthorDao authorDao) {
        this.userDao = userDao;
        this.bookmarkDao = bookmarkDao;
        this.bookDao = bookDao;
        this.authorDao = authorDao;
    }


    @Override
    public List<Author> getAuthors() {
        authenticate();
        return authorDao.getAuthors();
    }

    @Override
    public List<Bookmark> getBookmarks() {
        User user = authenticate();
        return bookmarkDao.getUserBookmarks(user);
    }

    @Override
    public List<Book> getBooks(long isbn, String bookName, int authorId, long firstTimestamp, long secondTimestamp) {
        authenticate();
        if (isbn != 0) return new ArrayList<>(Collections.singleton(bookDao.getBookByISBN(isbn)));
        boolean areTimestampsNull = firstTimestamp == 0 && secondTimestamp == 0;
        if (authorId != 0 && bookName == null && areTimestampsNull) { // only by authorId
            return bookDao.getBooksByAuthorId(authorId);
        }
        if (authorId == 0 && bookName != null && areTimestampsNull) { // only by bookName
            return bookDao.getBooksByName(bookName);
        }
        if (authorId != 0 && bookName != null && areTimestampsNull) { // author id and book name
            return bookDao.getBooksByAuthorIdAndBookName(authorId, bookName);
        }

        long first = firstTimestamp == 0 ? 1 : firstTimestamp;
        long second = secondTimestamp == 0 ? Integer.MAX_VALUE : secondTimestamp;
        if (authorId == 0 && bookName == null && !areTimestampsNull) { // only by timestamps
            return bookDao.getBooksByDateInterval(first, second);
        }
        if (authorId != 0 && bookName == null && !areTimestampsNull) { // author id and timestamps
            return bookDao.getBooksByAuthorIdAndDateInterval(authorId, first, second);
        }
        if (authorId == 0 && bookName != null && !areTimestampsNull) { // book name and timestamps
            return bookDao.getBooksByNameAndDateInterval(bookName, first, second);
        }
        if (authorId != 0 && bookName != null && !areTimestampsNull) {// all!
            return bookDao.getBooksByNameAndAuthorIdAndDateInterval(bookName, authorId, first, second);
        }
        throw new OperationException("You have not specified any parameter!");
    }

    @Override
    public List<User> getUsers(String namePattern) {
        User user = authenticate();
        checkAdminPermissions(user);
        return userDao.getUsersByNamePattern(namePattern);
    }

    @Override
    public boolean addUser(User targetUser) {
        User user = authenticate();
        checkAdminPermissions(user);
        return userDao.addUserToDatabase(targetUser);
    }

    @Override
    public boolean addBook(Book book) {
        User user = authenticate();
        checkAdminPermissions(user);
        return bookDao.addBookToDatabase(book);
    }

    @Override
    public boolean addBookmark(Book book, int page) {
        User user = authenticate();
        return bookDao.getBookByISBN(book.ISBN) != null && bookmarkDao.addBookmark(user, book, page);
    }

    @Override
    public boolean deleteBookmark(int id) {
        User user = authenticate();
        checkAdminPermissions(user);
        for (Bookmark bookmark : bookmarkDao.getUserBookmarks(user)) {
            if (bookmark.ID == id) {
                return bookmarkDao.deleteBookmarkById(id);
            }
        }
        throw new OperationException("Bookmark not found OR bookmark with specified id is not your own");
    }

    @Override
    public boolean deleteBook(long isbn) {
        User user = authenticate();
        checkAdminPermissions(user);
        return bookDao.deleteBookByIsbn(isbn);
    }

    @Override
    public boolean changeUserRole(User targetUser, User.Role targetRole) {
        User user = authenticate();
        checkAdminPermissions(user);
        if (!targetRole.hasParentOrEquals(User.Role.ADMIN)) {
            throw new OperationException("You can't give to user role that higher than your own");
        }
        return userDao.changeUserRole(targetUser, targetRole);
    }

    @Override
    public User getAuthenticatedUser() {
        return authenticate();
    }

    private boolean checkAdminPermissions(User user) {
        if (!user.ROLE.hasParentOrEquals(User.Role.ADMIN))
            throw new OperationException("You have no permissions to do that");
        return true;
    }

    private User authenticate() {
        Map headers = (Map) wsctx.getMessageContext().get(MessageContext.HTTP_REQUEST_HEADERS);
        try {
            if (headers.get("Authorization") == null)
                throw new AuthorizationException("No authorization header provided");
            String authHeader = headers.get("Authorization").toString();
            authHeader = authHeader.substring(1).substring(0, authHeader.length() - 2);
            if (!authHeader.startsWith("Basic ")) throw new AuthorizationException();
            String decoded = new String(Base64.decode(authHeader.substring(6)));
            String[] splitted = decoded.split(":");
            String username = splitted[0];
            String password = splitted[1];
            User user = userDao.getUserByNameAndPassword(username, password);
            if (user == null) throw new AuthorizationException("Invalid username or/and password");
            return user;
        } catch (Base64DecodingException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }
}
