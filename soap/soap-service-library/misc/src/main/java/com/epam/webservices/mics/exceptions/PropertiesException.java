package com.epam.webservices.mics.exceptions;

public class PropertiesException extends RuntimeException {
    public PropertiesException(String s) {
        super(s);
    }
}
