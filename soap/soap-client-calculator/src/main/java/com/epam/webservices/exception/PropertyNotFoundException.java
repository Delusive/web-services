package com.epam.webservices.exception;

public class PropertyNotFoundException extends Exception {
    public PropertyNotFoundException(String message) {
        super(message);
    }
}
