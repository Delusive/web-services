package com.epam.webservices;


import com.epam.webservices.exception.IllegalExpressionException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

class Main {
    private static final DisplayManager DISPLAY_MANAGER = new DisplayManager();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        DISPLAY_MANAGER.info(Constants.WELCOME_MSG);
        while (true) {
            String expr = in.nextLine();
            if (expr.equalsIgnoreCase(Constants.STRING_TO_EXIT)) break;
            try {
                ExpressionCalculator calc = new ExpressionCalculator(expr);
                double result = calc.evaluate();
                NumberFormat nf = new DecimalFormat(Constants.DECIMAL_FORMAT);
                String msg = Constants.RESULT_MSG
                        .replaceAll("%result%", nf.format(result))
                        .replaceAll("%exit%", Constants.STRING_TO_EXIT);
                DISPLAY_MANAGER.info(msg);
            } catch (IllegalExpressionException ex) {
                DISPLAY_MANAGER.info(Constants.INVALID_EXPR_MSG);
            }
        }
    }
}
