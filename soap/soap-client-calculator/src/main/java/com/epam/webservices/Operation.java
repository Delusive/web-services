package com.epam.webservices;

public enum Operation {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION
}
