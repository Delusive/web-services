package com.epam.webservices;


import com.epam.webservices.exception.IllegalExpressionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class ExpressionCalculator {
    private String expression;

    ExpressionCalculator(String expression) throws IllegalExpressionException {
        this.expression = expression;
        if (!isValid()) throw new IllegalExpressionException("Expression \"" + expression + "\" is invalid");
    }

    double evaluate() {
        //Избавляемся от умножения и деления:
        executeOperationSet(OperationSet.MULTIPLICATION_AND_DIVISION);
        //Складываем и вычитаем:
        executeOperationSet(OperationSet.ADDITION_AND_SUBTRACTION);
        return Double.parseDouble(expression);
    }

    private void executeOperationSet(OperationSet set) {
        outside:
        while (true) {
            for (int i = 0; i < expression.length(); i++) {
                char ch = expression.charAt(i);
                String pattern = "";
                Operation op = null;
                if (set.equals(OperationSet.ADDITION_AND_SUBTRACTION)) {
                    if (!(ch == '+' || ch == '-') || expression.charAt(i + 1) != ' ') continue;
                    pattern = ch == '+' ? Constants.PATTERN_FOR_ADDITION : Constants.PATTERN_FOR_SUBTRACTION;
                    op = ch == '+' ? Operation.ADDITION : Operation.SUBTRACTION;
                } else if(set.equals(OperationSet.MULTIPLICATION_AND_DIVISION)) {
                    if (!(ch == '*' || ch == '/')) continue;
                    pattern = ch == '*' ? Constants.PATTERN_FOR_MULTIPLICATION : Constants.PATTERN_FOR_DIVISION;
                    op = ch == '*' ? Operation.MULTIPLICATION : Operation.DIVISION;
                }
                int[] nums = getRightAndLeftNumbers(i);
                String item = calculateItem(nums[0], nums[1], op);
                expression = expression.replaceFirst(pattern, item);
                continue outside;
            }
            break;
        }
    }

    private String calculateItem(int first, int second, Operation operation) {
        int result = 0;
        switch (operation) {
            case MULTIPLICATION:
                result = Operations.multiplication(first, second);
                break;
            case DIVISION:
                result = Operations.division(first, second);
                break;
            case ADDITION:
                result = Operations.addition(first, second);
                break;
            case SUBTRACTION:
                result = Operations.subtraction(first, second);
                break;
        }
        return String.valueOf(result);
    }

    private int[] getRightAndLeftNumbers(int relativeChar) {
        int endOfFirst = relativeChar - 1;
        int beginOfSecond = relativeChar + 2;
        int beginOfFirst = endOfFirst - 1;
        while (beginOfFirst != -1 && expression.charAt(beginOfFirst) != ' ') {
            beginOfFirst--;
        }
        beginOfFirst++;
        int endOfSecond = beginOfSecond;
        while (endOfSecond != expression.length() && expression.charAt(endOfSecond) != ' ') {
            endOfSecond++;
        }
        int first = Integer.parseInt(expression.substring(beginOfFirst, endOfFirst));
        int second = Integer.parseInt(expression.substring(beginOfSecond, endOfSecond));
        return new int[]{first, second};
    }

    private boolean isValid() {
        String pattern = Constants.PATTERN_FOR_EXPRESSION;
        return !(expression == null || expression.isEmpty() || !expression.matches(pattern));
    }

    private enum OperationSet {
        ADDITION_AND_SUBTRACTION, MULTIPLICATION_AND_DIVISION
    }
}