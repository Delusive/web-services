package com.epam.webservices;

class Constants {
    private static final String CFG_FILE_NAME = "/calculator.properties";
    private static final String MSG_FILE_NAME = "/messages.properties";
    private static final PropertiesManager cfgProps = new PropertiesManager(CFG_FILE_NAME);
    private static final PropertiesManager msgProps = new PropertiesManager(MSG_FILE_NAME);

    //Config:
    static final String PATTERN_FOR_EXPRESSION = cfgProps.getProperty("pattern-for-expression");
    static final String PATTERN_FOR_ADDITION = cfgProps.getProperty("pattern-for-addition");
    static final String PATTERN_FOR_SUBTRACTION = cfgProps.getProperty("pattern-for-subtraction");
    static final String PATTERN_FOR_MULTIPLICATION = cfgProps.getProperty("pattern-for-multiplication");
    static final String PATTERN_FOR_DIVISION = cfgProps.getProperty("pattern-for-division");
    static final String DECIMAL_FORMAT = cfgProps.getProperty("decimal-format");
    static final String STRING_TO_EXIT = cfgProps.getProperty("string-to-exit");

    //Messages:
    static final String WELCOME_MSG = msgProps.getProperty("welcome");
    static final String RESULT_MSG = msgProps.getProperty("result");
    static final String INVALID_EXPR_MSG = msgProps.getProperty("invalid-expression");

}
