package com.epam.webservices;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Отвечает за вывод информации в консоль.
 */
class DisplayManager {
    private final Logger LOGGER = LogManager.getLogger();

    /**
     * Вывод сообщения в консоль
     *
     * @param msg Сообщение, которое необходимо вывести.
     */
    void info(String msg) {
        LOGGER.log(Level.INFO, msg);
    }

    /**
     * Вывод ошибки в консоль
     *
     * @param error Объект, унаследованный от Throwable
     */
    void error(Throwable error) {
        LOGGER.log(Level.ERROR, error);
    }
}
