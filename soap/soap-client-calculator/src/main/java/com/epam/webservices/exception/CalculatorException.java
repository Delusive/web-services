package com.epam.webservices.exception;

class CalculatorException extends Exception {
    CalculatorException(String message) {
        super(message);
    }
}
