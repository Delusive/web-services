package com.epam.webservices;

import com.epam.webservices.exception.PropertyNotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Отвечает за работу с properties-файлом
 */
class PropertiesManager {
    private final Properties properties = new Properties();
    private final DisplayManager displayManager = new DisplayManager();

    PropertiesManager(String fileName) {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        if (inputStream == null) {
            displayManager.error(new FileNotFoundException("File \"" + fileName + "\" not found!"));
            return;
        }
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            displayManager.error(e);
        }
    }

    /**
     * Получение значение параметра из файла конфигурации.
     *
     * @param key Параметр, значение которого необходимо получить.
     * @return Значение указанного параметра.
     */
    String getProperty(String key) {
        if (!properties.containsKey(key)) {
            displayManager.error(new PropertyNotFoundException("Property with key \"" + key + "\" not found!"));
            return null;
        }
        return properties.getProperty(key);
    }

}
