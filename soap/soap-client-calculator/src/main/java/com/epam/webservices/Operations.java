package com.epam.webservices;

import com.epam.webservices.soap.Calculator;
import com.epam.webservices.soap.CalculatorSoap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Operations {
    private static final Logger logger = LogManager.getLogger(Operations.class);
    private static CalculatorSoap calculator = new Calculator().getCalculatorSoap();


    static int addition(int a, int b) {
        logger.debug("Sending SOAP query");
        long start = System.currentTimeMillis();
        int result = calculator.add(a, b);
        long timeTaken = System.currentTimeMillis() - start;
        logger.debug("SOAP query completed in " + timeTaken + " milliseconds!" );
        return result;
    }

    static int subtraction(int a, int b) {
        logger.debug("Sending SOAP query");
        long start = System.currentTimeMillis();
        int result = calculator.subtract(a, b);
        long timeTaken = System.currentTimeMillis() - start;
        logger.debug("SOAP query completed in " + timeTaken + " milliseconds!" );
        return result;
    }

    static int division(int a, int b) {
        logger.debug("Sending SOAP query");
        long start = System.currentTimeMillis();
        int result = calculator.divide(a, b);
        long timeTaken = System.currentTimeMillis() - start;
        logger.debug("SOAP query completed in " + timeTaken + " milliseconds!" );
        return result;
    }

    static int multiplication(int a, int b) {
        logger.debug("Sending SOAP query");
        long start = System.currentTimeMillis();
        int result = calculator.multiply(a, b);
        long timeTaken = System.currentTimeMillis() - start;
        logger.debug("SOAP query completed in " + timeTaken + " milliseconds!" );
        return result;
    }

}
