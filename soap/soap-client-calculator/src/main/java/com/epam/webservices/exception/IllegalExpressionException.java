package com.epam.webservices.exception;

public class IllegalExpressionException extends CalculatorException {
    public IllegalExpressionException(String message) {
        super(message);
    }
}
